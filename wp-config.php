<?php
define('WP_CACHE', true); // Added by FlyingPress

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'boostz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qsqrPk ljK1^Rw}i$qdxFK*U,]xu7CMpdf6ehvKNi0DNqfee]rRj75M.di5IYhD%' );
define( 'SECURE_AUTH_KEY',  'o7p_Q{whP35*@3yCN~<qEH CdX>RfbOnj?D>IN>2^Lyes.[fnlC,x4[ZSVp@msTy' );
define( 'LOGGED_IN_KEY',    'aYW){ TtOZQl>}NvnMXS|A&SPo.Ko,5VTGcsK=3j`]Z.}#GmZ`}~ne]MkLJe ./Y' );
define( 'NONCE_KEY',        '&2+e<O@W1{1LNr!Ty<SxGQH%:w7t~u#@X?%T^Lkz4s}D2YjY##dWLP.5(egn^h9A' );
define( 'AUTH_SALT',        'itZ(@NRPwmkn1`_(t(M1RG_1UW2^IxuodJzICG%8QKiC]>xv~ZJE}(x?[pt-Vu.k' );
define( 'SECURE_AUTH_SALT', '-Z?ON!+,^6$ 1(+*#/O3J?5nB`BpkO$/pBZ0n>%2PUW#reeHHU@bU|[%D3AVS%tH' );
define( 'LOGGED_IN_SALT',   'kzLYn;DubltjpZ-4Zl_qKtpQ^[=(LZ|DnT>r^i5;VpYgb^/) #KJrs)F5@N.k{]T' );
define( 'NONCE_SALT',       '^)IeX;):eD~gBFTn9=;B h_bWTW!O#h&UHlQ@=GXbNKHcmt@?3-gPG+`6!r$:tlj' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false ); 
define( 'WP_DEBUG_DISPLAY', false ); 
/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
