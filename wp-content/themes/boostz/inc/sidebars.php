<?php
if ( function_exists( 'register_sidebar' ) ) {

	register_sidebar( array (
		'name' => __( 'First Footer Widget', 'onm_textdomain' ),
		'id' => 'first-footer-widget',
		'description' => __( 'First Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Second Footer Widget', 'onm_textdomain'),
		'id' => 'second-footer-widget',
		'description' => __( 'Second Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Third Footer Widget', 'onm_textdomain' ),
		'id' => 'third-footer-widget',
		'description' => __( 'Third Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Fourth Footer Widget', 'onm_textdomain' ),
		'id' => 'fourth-footer-widget',
		'description' => __( 'Fourth Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Fifth Footer Widget', 'onm_textdomain' ),
		'id' => 'fifth-footer-widget',
		'description' => __( 'Fifth Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Sixth Footer Widget', 'onm_textdomain' ),
		'id' => 'sixth-footer-widget',
		'description' => __( 'Sixth Footer Widget Area', 'onm_textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

}