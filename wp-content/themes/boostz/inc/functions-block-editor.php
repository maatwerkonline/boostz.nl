<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Enqueue the correct scripts for the Gutenberg block editor.
 */
function custom_block_editor_scripts() {
	wp_enqueue_script( 'custom_block_editor', TEMPPATH . '/assets/js/editor.js', array( 'wp-blocks', 'wp-dom' ), filemtime( TEMPPATH . '/assets/js/editor.js' ), true );
}
add_action( 'enqueue_block_editor_assets', 'custom_block_editor_scripts' );

/**
 * Enqueue the correct styles for the Gutenberg block editor.
 */
function block_editor_assets() {
	wp_enqueue_style( 'slick-slider', TEMPPATH . '/assets/css/slick.css' );
	wp_enqueue_script( 'slick-slider', TEMPPATH . '/assets/js/slick-slider.min.js', array('jquery'), '', false );
	wp_enqueue_style('global-styless-theme', TEMPPATH . '/assets/css/global-styles.css', array(), THEME_VERSION);
    wp_enqueue_style('theme-style', TEMPPATH . '/style.css', array(), THEME_VERSION);
	wp_enqueue_style('bootstrap', TEMPPATH . '/assets/css/bootstrap.min.css', array(), THEME_VERSION);
}
add_action( 'enqueue_block_editor_assets', 'block_editor_assets', 100 );