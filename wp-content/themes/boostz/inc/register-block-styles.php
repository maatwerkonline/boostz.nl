<?php

// Register Custom Block Styles here
// register_block_style(). @Link https://developer.wordpress.org/reference/functions/register_block_style/

register_block_style(            
  'core/button',            
   array(                
     'name'  => 'fill-secondary',                
     'label' => __( 'Fill Secondary', 'maatwerkonline' ),            
   )        
);

register_block_style(            
    'core/button',            
     array(                
       'name'  => 'outline-secondary',                
       'label' => __( 'Outline Secondary', 'maatwerkonline' ),            
    )        
);

register_block_style(            
  'core/button',            
   array(                
     'name'  => 'black-outline',                
     'label' => __( 'Black Outline', 'maatwerkonline' ),            
  )        
);