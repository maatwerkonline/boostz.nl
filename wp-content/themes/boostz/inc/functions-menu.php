<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class default_walker_nav_menu extends Walker_Nav_Menu {

	public function display_element($el, &$children, $max_depth, $depth = 0, $args, &$output){
		$id = $this->db_fields['id'];

		parent::display_element($el, $children, $max_depth, $depth, $args, $output);
	}

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// depth dependent classes
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth = ( $depth + 1); // because it counts the first submenu as 0

		$depth_classes = array(
			'dropdown-menu dropdown-menu-right',
			'menu-depth-' . $display_depth
		);
		$depth_class_names = implode( ' ', $depth_classes );
		// passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// build html
		$output .= "\n" . $indent . '<ul class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '" aria-labelledby="nav-menu-item-'. esc_attr($item->ID) . '">' . "\n";
	}

	// add main/sub classes to li's and links
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

		// Get ACF Setting 'nav_item_layout'
		$nav_item_layout = get_field('nav_item_layout', $item);

		// Passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		if($nav_item_layout == 'button-primary-fill'){
			$classes[] = 'wp-block-button is-style-fill';
		} elseif($nav_item_layout == 'button-primary-outline'){
			$classes[] = 'wp-block-button is-style-outline';
		} 
		elseif($nav_item_layout == 'button-secondary-fill'){
			$classes[] = 'is-style-fill-secondary';
		}elseif($nav_item_layout == 'button-secondary-fill'){
			$classes[] = 'is-style-fill-secondary';
		} elseif($nav_item_layout == 'button-secondary-outline'){
			$classes[] = 'is-style-outline-secondary';
		};
		$classes[] = (in_array('current-menu-item', $classes)) ? 'active' : '';
		$classes[] = (in_array('menu-item-has-children', $classes)) ? 'dropdown' : '';
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
		$is_mega_menu = (strpos($class_names,'mega') !== false) ? true : false;
		$mobile_dropdown = (strpos($class_names,'mobile-dropdown') !== false) ? true : false;
		$is_sidebar = (strpos($class_names,'menu-sidebar') !== false) ? true : false;
		$is_navbar_divider = (strpos($class_names,'navbar-divider') !== false) ? true : false;
		$is_dropdown_divider = (strpos($class_names,'dropdown-divider') !== false) ? true : false;
		$is_title = (strpos($class_names,' title') !== false) ? true : false;
		$no_title = (strpos($class_names,'no-title') !== false) ? true : false;
		$is_description = (strpos($class_names,'description') !== false) ? true : false;

		// Depth dependent classes
		$depth_classes = array(
			( $depth == 0 ? 'nav-item' : '' ),
			( $depth >=1 ? 'dropdown-item' : '' ),
			'nav-item-depth-' . $depth
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// build html
		if ($is_navbar_divider){
			$output .= $indent . '<li class="navbar-divider">';
		} elseif ($is_dropdown_divider){
			$output .= $indent . '<li class="dropdown-divider">';
		} else {
			$output .= $indent . '<li class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '">';
		}

		// link attributes
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="' . (($item->url[0] == "#" && !is_front_page()) ? esc_url(home_url()) : '') . esc_attr($item->url) .'"' : '';
		$attributes .= ( in_array( 'menu-item-has-children', $classes ) ? ' id="nav-menu-item-'. esc_attr($item->ID) . '" aria-haspopup="true" aria-expanded="false"' : '' );
		
		// link passed classes
		if($nav_item_layout != ''){
			$link_classes[] = 'wp-block-button__link';
		} else {
			$link_classes[] = 'nav-link';
		};
		$link_classes[] = (in_array( 'menu-item-has-children', $classes)) ? 'dropdown-toggle' : '';
		$link_class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $link_classes ), $item ) ) );
			
		// Build html
		$title_output = ($is_title) ? $item->title : '';
		$description_output = ($is_description) ? $item->description : '';
		$item_output = '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</span></a>'.$description_output;

		if ($is_sidebar) {
			ob_start();
			dynamic_sidebar($item->description);
			$sidebar_html = ob_get_clean();

			// build html
			$sidebar_output = '<div class="sidebar-menu-item">'.$sidebar_html.'</div>';
			$item_output = $sidebar_output;

		} elseif ($is_title){

			// build html
			$item_output = '<span class="' . $link_class_names . '" '. $attributes . '>' . $title_output . '</span>';

		} elseif ($is_navbar_divider || $is_dropdown_divider){

			// build html
			$item_output = '';

		} elseif ($is_description){

			// build html
			$item_output = (!$no_title) ? '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>'. $description_output : '<a class="' . $link_class_names . '" '. $attributes . '>'. $description_output . '</a>';

		} else{
			
			// build html
			$item_output = (!$no_title) ? '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>'. $description_output : $description_output;
		}

		// build html
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		$is_mega_menu = (strpos($class_names,'mega') !== false) ? true : false;

		$output .= "</li>\n";
	}

}

// Allow HTML descriptions in WordPress Menu
remove_filter( 'nav_menu_description', 'strip_tags' );
function nav_menu_description_item( $menu_item ) {
if ( isset( $menu_item->post_type ) ) {
	if ( 'nav_menu_item' == $menu_item->post_type ) {
		$menu_item->description = apply_filters( 'nav_menu_description', $menu_item->post_content );
	}
}

return $menu_item;
}
add_filter( 'wp_setup_nav_menu_item', 'nav_menu_description_item' );

// Add ACF fields to menu
function custom_menu_acf_fields() {
	
	acf_add_local_field_group(array(
		'key' => 'group_1',
		'title' => 'My Group',
		'fields' => array (
			array (
				'key' => 'nav_item_layout',
				'label' => 'Navigatie opmaak',
				'name' => 'nav_item_layout',
				'type' => 'select',
				'choices' => array(
					''	=> 'Link',
					'button-primary-fill'	   => 'Primary Button Style: Fill',
					'button-primary-outline'   => 'Primary Button Style: Outline',
					'button-secondary-fill'	   => 'Secondary Button Style: Fill',
					'button-secondary-outline' => 'Secondary Button Style: Outline',
				),
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'nav_menu_item',
					'operator' => '==',
					'value' => 'all',
				),
			),
		),
	));
	
}
add_action('acf/init', 'custom_menu_acf_fields');