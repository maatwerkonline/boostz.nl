<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();

?>

<?php if ( have_posts() ) : 
	while ( have_posts() ) : the_post();?>

		<article>
			<div class="wp-block-group alignfull corner-shape pt-0">
				<div class="wp-bootstrap-blocks-row row">
					<div class="col-12 p-0">

						<?php if ( function_exists('yoast_breadcrumb') ) {  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
							$gblock = get_post( 1247 );
							echo apply_filters( 'the_content', $gblock->post_content );
						}; ?>

					</div>
					<div class="col-12 col-md-1"></div>
					<div class="col-12 col-md-10">
						<h2 class="text-uppercase mb-0 d-inline">
							<?php echo get_the_title(); ?>
						</h2>
						<?php 
						$categories = wp_get_post_categories( $post->ID );
						if($categories) : ?>
							<div class="categories">
								<?php foreach($categories as $category) :
									$cat = get_category( $category );
									$cat_id = get_cat_ID( $cat->name );
									$cat_ids[] = get_cat_ID( $cat->name );
									?>
									<a href="<?php echo get_site_url(); ?>/boostz/blog/?term=<?php echo $cat_id; ?>" class="mr-3"># <?php echo $cat->name; ?></a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-12 col-md-1"></div>
				</div>
				<div class="wp-bootstrap-blocks-row row mt-5">
					<div class="col-12 col-md-1"></div>
					<div class="col-md-3 col-12 d-none d-md-block">
						<?php
							$reuse_block = get_post( 1811 );
							$reuse_block_content = apply_filters( 'the_content', $reuse_block->post_content);
							echo $reuse_block_content;
						?>
					</div>	
					<div class="col-md-7 col-12">
						
						<div class="content">
						
							<div class="post-meta mb-4">
							
							<div class="author-time d-flex align-items-center">
								<div class="author-avatar d-inline-block">
									<img src="<?php echo get_avatar_url(get_the_author_meta( 'ID' ), ['size' => '120']); ?>" width="60" height="60" class="d-inline avatar mr-2">
								</div>
								<div class="text-white">
									<div class="author"><span href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' , $post->post_author)); ?>" class="text-normal"><?php echo get_the_author_meta('display_name'); ?></span> </div>
									<div class="times"><p class="mb-0"><i class="bstz bstz-time"></i> <?php echo get_the_date( 'm-d-Y' ) ?></p>			</div>
								</div>
							</div>
							<!--
							<div class="shares d-flex align-items-center justify-content-end">
								<div class="social-share-icons-list d-flex align-items-center mb-0">
									<?php 
									$linkedin_count = get_click_count($post->ID, 'linkedin');
									$twitter_count = get_click_count($post->ID, 'twitter');
									$facebook_count = get_click_count($post->ID, 'facebook');
									$whatsapp_count = get_click_count($post->ID, 'whatsapp');
									$link_count = get_click_count($post->ID, 'link');
									$count = $facebook_count + $twitter_count + $linkedin_count + $whatsapp_count + $link_count; ?>

									<div class="social-icons-list list-inline list-unstyled mb-0">
										<span class="position-relative mr-1 d-none d-sm-inline"><?php if($linkedin_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $linkedin_count; ?></span><?php }; ?>
											<a class="share list-inline-item social linkedin" target="_blank" href="<?php echo esc_url('https://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'&amp;title='.urlencode(get_the_title())); ?>" data-click-count="linkedin">
												<i class="social-icons social-icons-linkedin"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-none d-sm-inline"><?php if($twitter_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $twitter_count; ?></span><?php }; ?>
											<a class="share list-inline-item social twitter" target="_blank" href="<?php echo esc_url('https://twitter.com/home?status='.(urlencode(__('Check this out:', 'maatwerkonline'))).' '.wp_get_shortlink()); ?>" data-click-count="twitter">
												<i class="social-icons social-icons-twitter"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-none d-md-inline"><?php if($facebook_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $facebook_count; ?></span><?php }; ?>
											<a class="share list-inline-item social facebook" target="_blank" href="<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink()); ?>" data-click-count="facebook">
												<i class="social-icons social-icons-facebook"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-none d-md-inline"><?php if($whatsapp_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $whatsapp_count; ?></span><?php }; ?>
											<a class="share list-inline-item social whatsapp" target="_blank" href="<?php echo esc_url('https://api.whatsapp.com/send?phone=whatsappphonenumber&text='.__('Check this out:', 'maatwerkonline').' '.wp_get_shortlink()); ?>" data-action="share/whatsapp/share" data-click-count="whatsapp">
												<i class="social-icons social-icons-whatsapp"></i>
											</a>
										</span>
										<span class="position-relative d-none d-md-inline"><?php if($link_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $link_count; ?></span><?php }; ?>
											<a class="share list-inline-item social link" data-click-count="link" data-clipboard-copied-text="<i class=&quot;lnr lnr-check&quot;></i>" data-clipboard-text="<?php echo wp_get_shortlink(); ?>">
												<i class="lnr lnr-link2"></i>
											</a>
										</span>
										<span class="position-relative d-md-none"><?php if($count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $count; ?></span><?php }; ?>
											<a tabindex="0" class="list-inline-item social link" data-toggle="popover" data-trigger="focus" data-bound="viewport" data-delay="{&quot;hide&quot;:&quot;500&quot;}" data-placement="bottom" data-html="true" data-content="
												<ul class='list-group list-group-flush'>
														<a class='share list-group-item btn btn-sm social linkedin d-sm-none' target='_blank' href='<?php echo esc_url('https://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'&amp;title='.urlencode(get_the_title())); ?>' data-click-count='linkedin'>
															<i class='social-icons social-icons-linkedin'></i> · <?php _e('Share on LinkedIn', 'maatwerkonline'); ?>
														</a>
														<a class='share list-group-item btn btn-sm social twitter d-sm-none' target='_blank' href='<?php echo esc_url('https://twitter.com/home?status='.(urlencode(__('Check this out:', 'maatwerkonline'))).' '.wp_get_shortlink()); ?>' data-click-count='twitter'>
															<i class='social-icons social-icons-twitter'></i> · <?php _e('Share on Twitter', 'maatwerkonline'); ?>
														</a>
														<a class='share list-group-item btn btn-sm social facebook' target='_blank' href='<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink()); ?>' data-click-count='facebook'>
															<i class='social-icons social-icons-facebook'></i> · <?php _e('Share on Facebook', 'maatwerkonline'); ?>
														</a>
														<a class='share list-group-item btn btn-sm social whatsapp' target='_blank' href='<?php echo esc_url('https://api.whatsapp.com/send?phone=whatsappphonenumber&text='.__('Check this out:', 'maatwerkonline').' '.wp_get_shortlink()); ?>' data-action='share/whatsapp/share' data-click-count='whatsapp'>
															<i class='social-icons social-icons-whatsapp'></i> · <?php _e('Share on WhatsApp', 'maatwerkonline'); ?>
														</a>
														<a class='share list-group-item btn btn-sm social link' data-click-count='link' data-clipboard-copied-text='<i class=&quot;lnr lnr-check&quot;></i> · <?php _e('Copied', 'maatwerkonline'); ?>' data-clipboard-text='<?php echo wp_get_shortlink(); ?>'>
															<i class='lnr lnr-link2'></i> · <?php _e('Copy Link', 'maatwerkonline'); ?>
														</a>
												</ul>
											"><i class="lnr lnr-ellipsis d-none d-sm-inline"></i><i class="lnr lnr-share4 d-sm-none"></i></a>
										</span>
									</div>

								</div>
							</div>

						</div>
						-->

						
						
							<?php the_content();?>
						</div>
						
					</div>
					<div class="col-12 col-md-1"></div>
				</div>
			</div>
		</article>

	<?php endwhile; 
endif;?>

<?php
	$reuse_block = get_post( 956 );
	$reuse_block_content = apply_filters( 'the_content', $reuse_block->post_content);
	echo $reuse_block_content;

	$reuse_block = get_post( 967 );
	$reuse_block_content = apply_filters( 'the_content', $reuse_block->post_content);
	echo $reuse_block_content;
?>

<?php get_footer();
