<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


// Constants
define('THEME_NAME', 'Dummy');
define('THEME_VERSION', '1.0.0');
define('TEMPPATH', get_template_directory_uri());
define('IMAGES', TEMPPATH . '/assets/images');


// Check if function theme_by_maatwerkonline_setup() exists
if ( ! function_exists( 'theme_by_maatwerkonline_setup' ) ) {
    
    /** 
     * Sets up theme defaults and registers support for various WordPress features. 
     * Note that this function is hooked into the after_setup_theme hook, which runs before the init hook. 
     * The init hook is too late for some features, such as indicating support for post thumbnails.
     */
    function theme_by_maatwerkonline_setup() {

        /** 
         * Load Theme Textdomain. Make theme available for translation. 
         * 
         * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
         */
        load_theme_textdomain( 'maatwerkonline', TEMPPATH . '/languages' );


        /** 
         * Register Nav Menus.
         * 
         * @link https://developer.wordpress.org/themes/functionality/navigation-menus/
         */
        register_nav_menus( array(
            'default' => __('Default Menu', 'maatwerkonline'),
            'topbar' => __('Topbar Menu', 'maatwerkonline'),
            'mobile' => __('Mobile Menu', 'maatwerkonline')
        ) );


        // Add Image Size
        // Would you like to add some Image Sizes? Add the add_image_size( $name, $width, $height, $crop ); right here. Documentation: https://developer.wordpress.org/reference/functions/add_image_size/

        /** 
         * Theme Support.
         * 
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/
         */
        include 'inc/theme-support.php';


        // Register Block Styles
        include 'inc/register-block-styles.php';


        // Load the Block Editor Functionalities
        include 'inc/functions-block-editor.php'; 


        // Functions Menu
        include 'inc/functions-menu.php';


        // Additional Custom Post Types
        // Looking for a place to add Additional Custom Post Types? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Custom Taxonomies
        // Looking for a place to add Additional Custom Taxonomies? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Fields
        // Looking for a place to add Additional Fields? We use the plugin 'Advance Custom Fields Pro' for this: /wp-admin/edit.php?post_type=acf-field-group

    }
}
add_action( 'after_setup_theme', 'theme_by_maatwerkonline_setup' );


/**
 * Add Styles.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 */
function theme_styles() {
    // If you would like to use Google Font (https://fonts.google.com/) you can add the font-css here
    // wp_enqueue_style('font-css','https://fonts.googleapis.com/css2?family=Open+Sans:wght@300,600&display=swap');

    // Fetch global styles for editor and front-end with theme.json
    wp_enqueue_style( 'slick-slider', TEMPPATH . '/assets/css/slick.css' );
    wp_enqueue_style( 'global-styles-theme', TEMPPATH . '/assets/css/global-styles.css' );
    wp_enqueue_style( 'bootstrap', TEMPPATH . '/assets/css/bootstrap.min.css', array(), THEME_VERSION );
    wp_enqueue_style( 'theme-style', TEMPPATH . '/style.css', array(), THEME_VERSION );
    wp_enqueue_style ( 'icons', TEMPPATH.'/assets/icons/bstz/bstz.css');
}
add_action('wp_enqueue_scripts', 'theme_styles');


/**
 * Add Scripts.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 */
function theme_scripts() {
    wp_enqueue_script( 'slick-slider', TEMPPATH . '/assets/js/slick-slider.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'bootstrap', TEMPPATH . '/assets/js/bootstrap.min.js', array(), '', true );
    wp_enqueue_script( 'init', TEMPPATH . '/assets/js/init.js', true );
    wp_localize_script( 'init', 'objectL10n', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'post_id' => get_the_ID()
    ) );
    
    // Only Enqueue Comment Reply Script on frontend if is single, if comments are open and if threaded comments are enabled. @link https://peterwilson.cc/including-wordpress-comment-reply-js/
    if ( is_singular() && comments_open() && get_option('thread_comments') ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'theme_scripts');

// Display user id on the main users page in the back-end
add_filter( 'manage_users_columns', 'column_register_wpse_101322' );
add_filter( 'manage_users_custom_column', 'column_display_wpse_101322', 10, 3 );

function column_register_wpse_101322( $columns ) 
{
    $columns['uid'] = 'ID';
    return $columns;
}

function column_display_wpse_101322( $empty, $column_name, $user_id ) 
{
    if ( 'uid' != $column_name )
        return $empty;

    return "$user_id";
}

// Register Sidebars
require_once __DIR__ . '/inc/sidebars.php';

// Add icomoon icons to gutenberg icons plugin
function add_my_icons($file) {
    $file = get_stylesheet_directory().'/assets/icons/bstz/bstz.json';
    return $file;
}

add_filter( 'jvm_richtext_icons_iconset_file', 'add_my_icons');

function add_my_css($cssfile) {
    $cssfile = TEMPPATH.'/assets/icons/bstz/bstz.css';
    return $cssfile;
}

add_filter( 'jvm_richtext_icons_css_file', 'add_my_css');

add_filter( 'jvm_richtext_icons_css_file', '__return_false');

// Enqueue style for the admin pages
function gutenberg_icomoon_icons() {
    wp_enqueue_style ( 'icons', TEMPPATH.'/assets/icons/bstz/bstz.css');
}

add_action( 'admin_enqueue_scripts', 'gutenberg_icomoon_icons' );

// Ajax filter functionalities
require_once 'template-parts/blocks/training-query/ajax-training-filter.php';
require_once 'template-parts/blocks/training-archive/ajax-training-archive.php';

//Dynamically populate field gravity forms
add_filter( 'gform_field_value_user_email', 'my_custom_population_function' );
function my_custom_population_function( $value ) {
    $current_user = wp_get_current_user();
    $user_email = $current_user ->user_email;
    return $user_email;
}

// Auto fill user role in gravity forms
add_filter('gform_field_value_user_role', 'gform_populate_user_role');
function gform_populate_user_role($value){
    $user = wp_get_current_user();
    $role = $user->roles;
    return reset($role);
}

// Auto fill user id in gravity forms
add_filter('gform_field_value_gebr_id', 'gform_populate_gebr_id');
function gform_populate_gebr_id($value){
    $gebr_id = get_current_user_id();
    return $gebr_id;
}

// Auto fill user subscription in gravity forms
add_filter('gform_field_value_user_sub', 'gform_populate_user_sub');
function gform_populate_user_sub($value){
    $user = wp_get_current_user();
    $role = $user->roles;
    $user_sub_lowercase = str_replace('-abonnement', '', $role[0]);
    $user_sub = ucfirst($user_sub_lowercase);
    return $user_sub;
}

add_filter( 'gform_pre_render_2', 'populate_posts' );
add_filter( 'gform_pre_validation_2', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_2', 'populate_posts' );
add_filter( 'gform_admin_pre_render_2', 'populate_posts' );
function populate_posts( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
 
        $choices = array('plus', 'gratis', 'premium');
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Post';
        $field->choices = $choices;
 
    }
 
    return $form;
}

