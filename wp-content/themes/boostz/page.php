<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php if ( !is_front_page( ) ) :
	// get reusable gutenberg block:
	$gblock = get_post( 1247 );
	echo apply_filters( 'the_content', $gblock->post_content );
endif;
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	<?php the_content();?>
<?php endwhile; endif;?>

<?php get_footer();
