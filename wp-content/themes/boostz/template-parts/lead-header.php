<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="lead-header">
    <header id="header">
        
        <nav class="navbar navbar-light" role="navigation">
            <div class="container">

                <div class="navbar-brand">
                    
                    <?php
                    
                    if ( function_exists( 'the_custom_logo' )  && get_theme_mod( 'custom_logo' ) != '' ) : // If we have Custom Logo uploaded in the Customizer 
                        
                        the_custom_logo();
                    
                    else:  // If we have NO Custom Logo uploaded in the Customer ?>

                        <a class="custom-logo-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

                            <p class="site-title mb-0 h6"><?php bloginfo( 'name' ); ?></p>

                            <?php $description = get_bloginfo( 'description', 'display' );
                            if ( $description || is_customize_preview() ) : // If we have a Site description ?>
                                <p class="site-description mb-0"><small><?php echo $description; ?></small></p>
                            <?php endif; ?>

                        </a>

                    <?php endif; ?>

                </div>

                <?php wp_nav_menu( array( 'menu' => 'default', 'theme_location' => 'default', 'container_id' => 'default-menu', 'container_class' => 'd-none d-lg-block mr-auto pl-5', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new default_walker_nav_menu()) ); ?>
                
                <?php if (is_user_logged_in()) { ?>
					<div class="wp-block-buttons">
						<div class="wp-block-button is-style-black-outline m-0 pr-0 pr-md-3 mb-3 mb-md-0"><a class="wp-block-button__link" href="<?php echo wp_logout_url() ?>"><i class="icon bstz bstz-person pl-0 pr-2" aria-hidden="true"></i><u>Uitloggen</u><i class="icon bstz bstz-double-arrow" aria-hidden="true"></i></a></div>
					</div>
					<div class="wp-block-buttons">
						<div class="wp-block-button is-style-btn-primary m-0 pt-3 pt-lg-0"><a class="wp-block-button__link" href="<?php get_site_url(); ?>/mijn-account/">Bekijk account <i class="icon bstz bstz-double-arrow" aria-hidden="true"> </i></a></div>
					</div>
				<?php } else { ?>
					<div class="wp-block-buttons">
						<div class="wp-block-button is-style-black-outline m-0 pr-0 pr-md-3 mb-3 mb-md-0"><a class="wp-block-button__link" href="<?php get_site_url(); ?>/mijn-account/"><i class="icon bstz bstz-person pl-0 pr-2" aria-hidden="true"></i><u>Inloggen</u><i class="icon bstz bstz-double-arrow" aria-hidden="true"></i></a></div>
					</div>
					<div class="wp-block-buttons">
						<div class="wp-block-button is-style-btn-primary m-0 pt-3 pt-lg-0"><a class="wp-block-button__link" href="<?php get_site_url(); ?>/abonnement/">Probeer nu gratis! <i class="icon bstz bstz-double-arrow" aria-hidden="true"> </i></a></div>
					</div>
				<?php } ?>
                
                <div id="menu-toggle" class="d-block d-lg-none <?php if(is_page('offline')){ ?> inactive <?php }; ?>">
                    <div class="hamburger">
                        <div class="hamburger-box">
                            <?php if(is_page('offline')){ ?>
                                <i class="lnr-undo2"></i>
                            <?php } else { ?>
                                <div class="hamburger-inner"></div>
                            <?php }; ?>
                        </div>
                    </div>
                    <span class="mobile-menu-label">
                        <?php if(is_page('offline')){
                                _e('Reload', 'maatwerkonline'); 
                             } else { ?>
                                <div class="menu-bars">
                                    <div class="menu-bar menu-bar-1"></div>
                                    <div class="menu-bar menu-bar-2"></div>
                                    <div class="menu-bar menu-bar-3"></div>
                                </div>
                        <?php }; ?>
                    </span>
                </div>

            </div>
        </nav>
        
    </header>
</div>