<?php
/**
 * Block Name: Breadcrumbs
 * This is the template that displays the block breadcrumbs.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Breadcrumbs -->
	<?php if ( function_exists('yoast_breadcrumb') ) {  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
	yoast_breadcrumb( '<p class="mb-0 py-3 breadcrumb" id="breadcrumbs">','</p>' );
	}; ?>
</div>