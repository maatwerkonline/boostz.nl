<?php
/**
 * Block Name: Course USPS
 * This is the template that displays the block course-usps.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

if(!empty($_GET['post'])) {
	$course = $_GET['post'];
} else {
	$course = get_the_id();
}

$lessons = course_lessons($course, 'published');
$lesson_count = count($lessons);

$total_duration = get_field('total_duration');
$customer_grade = get_field('customer_grade');

?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> usps"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<div class="usps--lessons">
		<svg xmlns="http://www.w3.org/2000/svg" width="22.594" height="16.648" viewBox="0 0 22.594 16.648">
			<g id="camera-video" transform="translate(-0.975 -5.475)">
				<path id="Path_125" data-name="Path 125" d="M4.346,7.111A1.734,1.734,0,0,0,2.611,8.846v9.907a1.734,1.734,0,0,0,1.735,1.735h9.907a1.734,1.734,0,0,0,1.735-1.735V8.846a1.734,1.734,0,0,0-1.735-1.735H4.346ZM1.125,8.846A3.221,3.221,0,0,1,4.346,5.625h9.907a3.221,3.221,0,0,1,3.221,3.221v9.907a3.221,3.221,0,0,1-3.221,3.221H4.346a3.221,3.221,0,0,1-3.221-3.221Z" stroke="#000" stroke-width="0.3"/>
				<path id="Path_126" data-name="Path 126" d="M24.93,10.69,29.044,8.3a.473.473,0,0,1,.717.391V19.67a.473.473,0,0,1-.717.392l-4.113-2.385-.746,1.285L28.3,21.347a1.959,1.959,0,0,0,2.949-1.678V8.695A1.959,1.959,0,0,0,28.3,7.018L24.187,9.4l.746,1.285Z" transform="translate(-7.828 -0.383)" stroke="#000" stroke-width="0.3"/>
			</g>
		</svg>
		<h3><?php echo $lesson_count; ?> <?php _e('lessons','maatwerkonline') ?></h3>
	</div>
	<?php if($total_duration) : ?>
		<div class="usps--duration">
			<svg xmlns="http://www.w3.org/2000/svg" id="time" width="20.078" height="20.078" viewBox="0 0 20.078 20.078">
				<g id="Ellipse_14" data-name="Ellipse 14" fill="none" stroke="#000" stroke-width="2">
					<circle cx="10.039" cy="10.039" r="10.039" stroke="none"/>
					<circle cx="10.039" cy="10.039" r="9.039" fill="none"/>
				</g>
				<path id="Path_23" data-name="Path 23" d="M686.457,1531.446v6.418l3.871,2.343" transform="translate(-676.709 -1527.396)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="2"/>
			</svg>
			<h3><?php echo $total_duration; ?></h3>
		</div>
	<?php endif; ?>
	<?php if($customer_grade) : ?>
		<div class="usps--grade">
			<svg xmlns="http://www.w3.org/2000/svg" width="23.135" height="21.706" viewBox="0 0 23.135 21.706">
				<g id="star" transform="translate(0.154 -0.972)">
					<path id="Path_127" data-name="Path 127" d="M4.089,21.591a.757.757,0,0,0,1.064.846l6.261-3.218,6.26,3.218a.757.757,0,0,0,1.064-.844l-1.183-6.746,5.025-4.786a.793.793,0,0,0-.4-1.355L15.19,7.712,12.074,1.54a.732.732,0,0,0-1.322,0L7.637,7.713.651,8.706a.793.793,0,0,0-.4,1.355l5.025,4.786L4.089,21.593Zm7-3.947-5.257,2.7L6.817,14.7a.806.806,0,0,0-.233-.72L2.44,10.031l5.779-.822A.749.749,0,0,0,8.78,8.8l2.634-5.217L14.047,8.8a.749.749,0,0,0,.56.411l5.779.82-4.144,3.95a.8.8,0,0,0-.233.722L17,20.345l-5.257-2.7a.718.718,0,0,0-.657,0Z" transform="translate(0)" fill="#ff6400" stroke="#ff6400" stroke-width="0.3"/>
				</g>
			</svg>
			<h3><?php echo $customer_grade; ?></h3>
		</div>
	<?php endif; ?>
</div>