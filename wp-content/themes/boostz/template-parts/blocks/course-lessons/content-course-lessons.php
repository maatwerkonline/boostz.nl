<?php
/**
 * Block Name: Course Lessons
 * This is the template that displays the block course-lessons.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

if(!empty($_GET['post'])) {
	$course = $_GET['post'];
} else {
	$course = get_the_id();
}

$lessons = course_lessons($course, 'published');

?>
<div class="lessons-loop">
	<?php
	$i = 0;
	foreach ($lessons as $lesson) : $i++;
		$completed = 0;
		$user = wp_get_current_user();
		if ( !in_array( 'gratis-abonnement', (array) $user->roles ) && !in_array( 'uitgeschreven', (array) $user->roles ) && is_user_logged_in() || $i == 1  ) {
			$completed = 1;
		}
	?>
	<?php if($completed == 1) : ?>
		<a href="<?php echo get_the_permalink($lesson->ID); ?>" class="lesson-loop--row row active">
		<div class="video-lesson d-none">
			<?php 
			$lesson_link = get_field( 'lesson_video_link', $lesson->ID );
			if ($lesson_link) { ?>
				<video src="<?php echo $lesson_link; ?>" controls controlsList="nodownload"></video>
			<?php } else {
				echo $lesson_video_embed = get_post_meta( $lesson->ID, '_lesson_video_embed', true );
			}
			?>
		</div>	
	<?php else: ?>
		<div class="lesson-loop--row row">
	<?php endif; ?>	
			<div class="col-2 d-flex">
				<?php if($completed == 1) : ?>
					<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" class="my-auto">
						<g id="play_-_small" data-name="play - small" transform="translate(1 1)">
							<circle id="Ellipse_13" data-name="Ellipse 13" cx="10" cy="10" r="10" fill="none" stroke="#ff6400" stroke-width="2"/>
							<path id="Path_22" data-name="Path 22" d="M653.612,1533.743l-3.489-2.909a.649.649,0,0,0-1.064.5v5.818a.649.649,0,0,0,1.064.5l3.489-2.909A.648.648,0,0,0,653.612,1533.743Z" transform="translate(-640.738 -1524.343)" fill="#ff6400"/>
						</g>
					</svg>
				<?php else: ?>	
					<svg xmlns="http://www.w3.org/2000/svg" id="lock" width="19.119" height="26.941" viewBox="0 0 19.119 26.941" class="my-auto">
						<path id="Path_122" data-name="Path 122" d="M21.268,14.161H9.1A1.738,1.738,0,0,0,7.363,15.9v8.691A1.738,1.738,0,0,0,9.1,26.328H21.268a1.738,1.738,0,0,0,1.738-1.738V15.9A1.738,1.738,0,0,0,21.268,14.161ZM9.1,12.423A3.476,3.476,0,0,0,5.625,15.9v8.691A3.476,3.476,0,0,0,9.1,28.066H21.268a3.476,3.476,0,0,0,3.476-3.476V15.9a3.476,3.476,0,0,0-3.476-3.476Zm0-5.214a6.083,6.083,0,1,1,12.167,0v5.214H19.53V7.208a4.345,4.345,0,1,0-8.691,0v5.214H9.1Z" transform="translate(-5.625 -1.125)" fill="#fff" fill-rule="evenodd"/>
					</svg>
				<?php endif; ?>
			</div>
			<div class="col-8 d-flex">
				<h6 class="my-auto">
						<?php echo $i . '. ' . $lesson->post_title; ?>
				</h6>
			</div>
			<div class="col-2 d-flex">
				<h6 class="my-auto">
					<?php 
					$length = get_post_meta($lesson->ID, '_lesson_length', true);
					$hours = floor($length / 60).':'.str_pad(($length % 60), 2, "0", STR_PAD_LEFT);
					echo $hours;
					?>
				</h6>
			</div>
		<?php 
		$user = wp_get_current_user();
		if ( !in_array( 'gratis-abonnement', (array) $user->roles ) && !in_array( 'uitgeschreven', (array) $user->roles ) && is_user_logged_in() ) {
			$completed = 1 ;
		}
		if($completed == 1) : ?>
		</a>
	<?php else: ?>
		</div>
	<?php endif;
	endforeach
	?>
</div>

