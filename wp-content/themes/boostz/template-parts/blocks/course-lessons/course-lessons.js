(function ($) {
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockcourselessons = function () {
		$('.lesson-loop--row').on('click', function (e) {
			e.preventDefault();
			var video = $(this).children('.video-lesson').html();
			$('.course-video-frame .video-frame-holder').html(video);
			if ($(this).hasClass('active')) {
				$('.lesson-loop--row').removeClass('current');
				$(this).addClass('current');
			}
			if (!$(this).hasClass("active")) {
				var sub_message = $('.video-frame .video-frame-message').html()
				console.log('test');
				$('.course-video-frame.video-frame .video-frame-holder').html(sub_message);
			}
		});

		$(".lesson-loop--row").hasClass
		if ($(".lesson-loop--row:first").hasClass("active")) {
			$(".lesson-loop--row:first").addClass("current")
			var video = $(".lesson-loop--row:first").children('.video-lesson').html();
			$('.course-video-frame.video-frame .video-frame-holder').html(video);
		}
	}

	// Initialize dynamic block preview (editor).
	if (window.acf) {
		window.acf.addAction('render_block_preview/type=course-lessons', initializeBlockcourselessons);
	} else {
		$(document).ready(function () {
			initializeBlockcourselessons();
		});
	}
})(jQuery);
