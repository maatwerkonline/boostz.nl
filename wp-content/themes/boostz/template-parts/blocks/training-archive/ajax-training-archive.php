<?php 
/**
 * The file for the Ajax training archive Functionality
 *
 * This file works together with the ajax-training-archive.js file in the block folder
 *
 * @see 	    https://rudrastyh.com/wordpress/load-more-posts-ajax.html
 * @author 		Maatwerk Online
 * @version     1.0.0
 */
		
// The function for the Ajax Call
function training_archive_handler(){
	
    $category = $_POST['category'];

    if ($category == 'alles'){
		// If archive is not set use this args
		$args = array(
			'post_type' => 'course',
			'post_status' => 'publish',
			'posts_per_page' => -1
		);
	}else{
		// If archive is set use this args
		$args = array(
			'post_type' => 'course',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'course-category', //double check your taxonomy name in you dd 
					'field'    => 'slug',
					'terms'    => $category,
				),
			),
		);
	}
	
    // Prepare our arguments for the query
    $query = new WP_Query( $args );

    include('training-archive-content.php');

    die; // Here we exit the script and even no wp_reset_query() required

}
add_action('wp_ajax_training_archive', 'training_archive_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_training_archive', 'training_archive_handler'); // wp_ajax_nopriv_{action}