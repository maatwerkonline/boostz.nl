(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockreviewslide = function() {        
            $('.review-slider-container').slick({
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: true
            });
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=review-slider', initializeBlockreviewslide );
	} else {
		$(document).ready(function(){
			initializeBlockreviewslide();
		});
	}
})(jQuery);
					