<?php
/**
 * Block Name: Review slider
 * This is the template that displays the block review-slider.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> position-relative"> <!-- Review slider -->
	<?php
	$reviews = get_field('review_slider');
	$review_title = get_field('review_title');
	?>
	<h4 class="review-slider--title"><?php echo $review_title; ?></h4>
	<div class="review-slider-container mt-4">
		<?php
		foreach ($reviews as $review){
			$content = $review['review_content'];
			$image = $review['review_image'];
			$name = $review['review_name'];
			$function = $review['review_function'];
			?>
			<div class="review-card d-flex">
				<div class="my-auto d-flex">
					<img class="mr-3 d-none d-md-block" src="https://boostznl-w107c2.preview.wpmanaged.nl/wp-content/uploads/2021/11/qoute.png">
					<p class="mb-0 review-card--content"><?php echo $content; ?></p>
				</div>
				<div class="review-card--person-card">
					<img src="<?php echo $image; ?>">
					<h6 class="text-white"><?php echo $name; ?></h6>
					<p class="text-white"><?php echo $function; ?></p>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>