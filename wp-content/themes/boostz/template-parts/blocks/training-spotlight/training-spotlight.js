(function ($) {
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockspotlight = function () {
		$('.spotlight-slider').slick({
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			prevArrow: "<i class='bstz bstz-double-arrow-back prev' aria-hidden='true'></i>",
			nextArrow: "<i class='bstz bstz-double-arrow next' aria-hidden='true'></i>",
			dots: true,
			responsive: [
				{
					breakpoint: 772,
					settings: {
						arrows: false
					}
				}
			]
		});
	}

	// Initialize dynamic block preview (editor).
	if (window.acf) {
		window.acf.addAction('render_block_preview/type=training-spotlight', initializeBlockspotlight);
	} else {
		$(document).ready(function () {
			initializeBlockspotlight();
		});
	}

	var spotlightHeight = 0;
	// Div's class
	$(".spotlight-card").each(function () {
		if ($(this).height() > spotlightHeight) { spotlightHeight = $(this).height(); }
	});
	$(".spotlight-card").height(spotlightHeight);
})(jQuery);

