<?php
/**
 * Block Name: Training spotlight
 * This is the template that displays the block training-spotlight.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) { 
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> position-relative"> <!-- Training spotlight block -->
	<?php
	$title = get_field('spotlight_slider_title');
	$args = array(
		'post_type' => 'course',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_course_featured',
				'value' => 'featured',
				'compare' => '='
			)                   

		)
	);

	$query = new WP_Query($args);
	?>
	<h4 class="spotlight-slider--title"><?php echo $title; ?></h4>
	<div class="spotlight-slider">
		<?php
		while ( $query->have_posts() ) {
			$query->the_post();
			// Get course duration
			$duration = get_post_meta(get_the_ID(  ), 'course_duration');
			$characteristics = get_post_meta(get_the_ID(  ), 'characteristics');
			// Calculate the total amount of lessons in the course
			$lesson_array = get_post_meta(get_the_ID(), '_lesson_order');
			$clean_array = explode(',', $lesson_array[0]);
			$lesson_count = count($clean_array);
			?>
			<div class="super-container">
				<div class="spotlight-card">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="featured-image">
					<h5 class="text-white"><?php echo get_the_title(); ?></h5>
					<h6><?php echo $characteristics[0]; ?></h6>
					<p><i class="bstz bstz-time pr-2"></i><?php echo $duration[0]; ?> min</p>
					<?php 
						if($lesson_count > 1):
						echo '<p><i class="bstz bstz-camera pr-1"></i>' . $lesson_count . ' Lessen</p>';
						else:
							echo '<p><i class="bstz bstz-camera pr-1"></i>' . $lesson_count . ' Les</p>';
						endif;
					?>
					<p class="py-2"><?php echo get_the_excerpt(); ?></p>
					<div class="py-3 position-relative">
						<a class="btn-white" href="<?php echo get_permalink(); ?>"><?php _e('Bekijk deze training', 'maatwerkonline'); ?><i class="bstz bstz-double-arrow pl-3"></i></a>
					</div>
				</div>
			</div>
			<?php
		}
		?>
	</div>

</div>