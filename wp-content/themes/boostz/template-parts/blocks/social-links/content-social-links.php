<?php
/**
 * Block Name: Social links
 * This is the template that displays the block social-links.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php
	$social_links = get_field('social_link');
	?>
	<div class="social-links d-flex">
		<?php
		foreach($social_links as $social_link){
		?>
			<a href="<?php echo $social_link['link']; ?>" class="social-link" target="_blank"><?php echo $social_link['icon']; ?></a>
		<?php
		}
		?>
	</div>
</div>