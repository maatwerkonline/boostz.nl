<?php
/**
 * Block Name: Header
 * This is the template that displays the block header.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>

<div id="wrapper" class="d-flex">
	<?php wp_nav_menu( array( 'menu' => 'mobile', 'theme_location' => 'mobile', 'container_id' => 'sidebar-wrapper', 'container_class' => 'bg-dark border-right', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
	<div class="sidebar">
</div>	
	<div id="page-content-wrapper">

		<?php if ( !is_page_template( 'page-leadpage.php' ) ) :

			get_template_part('template-parts/header');

		endif; ?>