<?php
/**
 * Block Name: Course Teacher Information
 * This is the template that displays the block course-teacher-information.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

if(!empty($_GET['post'])) {
	$course = $_GET['post'];
} else {
	$course = get_the_id();
}

$teacher_id = get_the_author_ID($course);
$teacher_image = mt_profile_img($teacher_id, array('size' => 'small', 'echo' => false ));
// Get the teachers name
$teacher_name = get_author_name($teacher_id);

$teacher_meta = get_user_meta( $teacher_id );
$teacher_functions = nl2br(get_the_author_meta( 'teacher_functions', $teacher_id ));
$teacher_description = nl2br(get_the_author_meta( 'description', $teacher_id ));

?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> teacher-information row"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<div class="col-md-3 col-12 teacher-information--image">
		<?php echo $teacher_image; ?>
	</div>
	<div class="col-md-9 col-12">
		<h4 class="mb-4"><?php _e('Meet your teacher','maatwerkonline') ?></h4>
		<h5 class="mb-0 teacher-information--name"><strong><?php echo $teacher_name; ?></strong></h5>
		<h6 class="pb-4"><?php echo $teacher_functions ; ?></h6>
		<p><?php echo $teacher_description; ?></p>
	</div>
</div>