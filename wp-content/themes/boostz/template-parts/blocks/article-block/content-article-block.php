<?php
/**
 * Block Name: Article Block
 * This is the template that displays the block article-block.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

?>
 
<div class="article">
	<div class="article__title">
		<InnerBlocks />
	</div>	
	<div class="article__content">
	</div>	
</div>
<?php
wp_reset_postdata();
?>