(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockpartner = function() {
        
            $('.partners-slider').slick({
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                arrows: false,
                dots: true,
                responsive: [
                    {
                        breakpoint: 772,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=partner-slider', initializeBlockpartner );
	} else {
		$(document).ready(function(){
			initializeBlockpartner();
		});
	}
})(jQuery);
					