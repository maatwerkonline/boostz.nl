<?php
/**
 * Block Name: Partner slider
 * This is the template that displays the block partner-slider.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Partner slider -->
	<div class="partners-slider py-5">
		<?php
		$slider_images = get_field('slider_images');
		foreach ($slider_images as $slider_image){
			?>
			<div class="d-flex">
				<img class="slider-image m-auto" src="<?php echo $slider_image['image']; ?>">
			</div>
			<?php 
		}; ?>
	</div>
</div>