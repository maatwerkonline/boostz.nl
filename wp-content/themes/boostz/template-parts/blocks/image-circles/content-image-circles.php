<?php
/**
 * Block Name: Image circles
 * This is the template that displays the block image-circles.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> position-relative"> <!-- Image circles -->
	<?php
	$image_1 = get_field('image_1');
	$image_2 = get_field('image_2');
	$image_3 = get_field('image_3');

	$image_pos_1 = get_field('image_position_1');
	$image_pos_2 = get_field('image_position_2');
	$image_pos_3 = get_field('image_position_3');
	
	?>
	<div class="circles-container d-none d-md-flex">
		<div class="d-flex flex-column row-one">
			<div class="circle orange medium">
				<div class="circle-inside"></div>
			</div>
		</div>
		<div class="d-flex flex-column row-two">
			<div class="circle black small">
				<div class="circle-inside"></div>
			</div>
			<div class="circle image large" style="background-image:url('<?php echo $image_1; ?>'); background-position: <?php echo $image_pos_1; ?>%;"></div>
			<div class="circle orange filled small">
				<div class="circle-inside"></div>
			</div>
		</div>
		<div class="d-flex flex-column row-three">
			<div class="circle orange filled small">
				<div class="circle-inside"></div>
			</div>
			<div class="circle image extra-large" style="background-image:url('<?php echo $image_2; ?>'); background-position: <?php echo $image_pos_2; ?>%;"></div>
			<div class="circle orange medium">
				<div class="circle-inside"></div>
			</div>
		</div>
		<div class="d-flex flex-column row-four">
			<div class="circle black large">
				<div class="circle-inside"></div>
			</div>
			<div class="circle white small filled">
				<div class="circle-inside"></div>
			</div>
			<div class="circle image large" style="background-image:url('<?php echo $image_3; ?>'); background-position: <?php echo $image_pos_3; ?>%;"></div>
		</div>
	</div>

	<div class="circles-container-mobile d-flex d-md-none">
		<div class="d-flex flex-column row-one-mobile">
			<div class="circle image large" style="background-image:url('<?php echo $image_1; ?>'); background-position: <?php echo $image_pos_1; ?>%;"></div>
		</div>
		<div class="d-flex flex-column row-two-mobile">
			<div class="circle orange medium faded">
				<div class="circle-inside"></div>
			</div>
			<div class="circle image extra-large" style="background-image:url('<?php echo $image_2; ?>'); background-position: <?php echo $image_pos_2; ?>%;"></div>
		</div>
		<div class="d-flex flex-column row-three-mobile">
			<div class="circle blue large faded">
				<div class="circle-inside"></div>
			</div>
			<div class="circle white small filled">
				<div class="circle-inside"></div>
			</div>
			<div class="circle orange medium">
				<div class="circle-inside"></div>
			</div>
		</div>
	</div>

</div>