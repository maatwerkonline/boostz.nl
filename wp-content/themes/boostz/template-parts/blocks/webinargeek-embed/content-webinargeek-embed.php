<?php
/**
 * Block Name: Webinargeek embed
 * This is the template that displays the block webinargeek-embed.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?> webinargeek" class="<?php echo esc_attr($classes); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php
	$webinar_source = get_field('webinar_source');
	$title = get_field('refuse_title', 1413);
	$description = get_field('refuse_description', 1413);
	$button = get_field('refuse_button_text', 1413);
	$button_link = get_field('refuse_button_link', 1413);
	$user = wp_get_current_user();
	if ( in_array( 'gratis-abonnement', $user->roles ) || in_array( 'plus-abonnement', $user->roles ) || in_array( 'premium-abonnement', $user->roles ) || in_array( 'administrator', $user->roles ) ) {
		echo $webinar_source;
	} else { ?>
		<div class="webinar-noclick">
			<div class="noclick-container"></div>
			<div class="noclick-message">
				<h4><?php echo $title; ?></h4>
				<p><?php echo $description; ?></p>
				<div class="wp-block-button is-style-btn-primary m-0 pt-3">
					<a class="wp-block-button__link" href="<?php echo $button_link; ?>"><?php echo $button; ?><i class="icon bstz bstz-double-arrow" aria-hidden="true"> </i></a>
				</div>
			</div>
			<?php echo $webinar_source; ?>
		</div>
	<?php }
	
	?>


</div>