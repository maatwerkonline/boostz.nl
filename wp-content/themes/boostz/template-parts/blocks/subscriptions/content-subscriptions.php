<?php
/**
 * Block Name: Subscriptions
 * This is the template that displays the block subscriptions.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Subscription block -->
	<?php
	$subscriptions_title = get_field('subscriptions_title');
	$subscriptions_description = get_field('subscriptions_description');
	$sub_1_title = get_field('sub_1_title');
	$sub_1_description = get_field('sub_1_description');
	$sub_1_usp_icon_1 = get_field('sub_1_usp_icon_1');
	$sub_1_usp_1 = get_field('sub_1_usp_1');
	$sub_1_usp_icon_2 = get_field('sub_1_usp_icon_2');
	$sub_1_usp_2 = get_field('sub_1_usp_2');
	$sub_1_usp_icon_3 = get_field('sub_1_usp_icon_3');
	$sub_1_usp_3 = get_field('sub_1_usp_3');
	$sub_1_button = get_field('sub_1_button');

	$sub_2_title = get_field('sub_2_title');
	$sub_2_description = get_field('sub_2_description');
	$sub_2_usp_icon_1 = get_field('sub_2_usp_icon_1');
	$sub_2_usp_1 = get_field('sub_2_usp_1');
	$sub_2_usp_icon_2 = get_field('sub_2_usp_icon_2');
	$sub_2_usp_2 = get_field('sub_2_usp_2');
	$sub_2_usp_icon_3 = get_field('sub_2_usp_icon_3');
	$sub_2_usp_3 = get_field('sub_2_usp_3');
	$sub_2_button = get_field('sub_2_button');

	$sub_3_title = get_field('sub_3_title');
	$sub_3_description = get_field('sub_3_description');
	$sub_3_usp_icon_1 = get_field('sub_3_usp_icon_1');
	$sub_3_usp_1 = get_field('sub_3_usp_1');
	$sub_3_usp_icon_2 = get_field('sub_3_usp_icon_2');
	$sub_3_usp_2 = get_field('sub_3_usp_2');
	$sub_3_usp_icon_3 = get_field('sub_3_usp_icon_3');
	$sub_3_usp_3 = get_field('sub_3_usp_3');
	$sub_3_button = get_field('sub_3_button');

	?>
	<div class="row">
		<div class="col-12 text-center">
			<h4 class=""><?php echo $subscriptions_title; ?></h4>
			<p class="px-5"><?php echo $subscriptions_description; ?></p>
		</div>
	</div>
	<div class="row pt-5">
		<div class="col-lg-4 col-12 d-flex">
			<div class="subscription-card smaller mx-auto mr-lg-0 my-lg-auto mb-2">
				<h4 class="mx-auto mb-0"><?php echo $sub_1_title; ?></h4>
				<p class="mx-auto"><?php echo $sub_1_description; ?></p>
				<ul class="subscription-card--list">
					<li class="plus"><p><?php echo $sub_1_usp_icon_1 . $sub_1_usp_1; ?></p></li>
					<li class="min"><p><?php echo $sub_1_usp_icon_2 . $sub_1_usp_2; ?></p></li>
					<li class="min"><p><?php echo $sub_1_usp_icon_3 . $sub_1_usp_3; ?></p></li>
				</ul>
				<div class="my-3 mx-auto">
					<a href="<?php get_site_url(); ?>/abonnement/" class="btn-orange-outlined"><?php echo $sub_1_button; ?><i class="bstz bstz-double-arrow"></i></a>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12 d-flex">
			<div class="subscription-card large my-2 mx-auto m-lg-auto">
				<h4 class="mx-auto mb-0"><?php echo $sub_2_title; ?></h4>
				<p class="mx-auto"><?php echo $sub_2_description; ?></p>
				<ul class="subscription-card--list">
					<li class="plus"><p><?php echo $sub_2_usp_icon_1 . $sub_2_usp_1; ?></p></li>
					<li class="plus"><p><?php echo $sub_2_usp_icon_2 . $sub_2_usp_2; ?></p></li>
					<li class="min"><p><?php echo $sub_2_usp_icon_3 . $sub_2_usp_3; ?></p></li>
				</ul>
				<div class="my-3 mx-auto">
					<a href="<?php get_site_url(); ?>/abonnement/" class="btn-blue"><?php echo $sub_2_button; ?><i class="bstz bstz-double-arrow"></i></a>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12 d-flex">
			<div class="subscription-card smaller mx-auto ml-lg-0 mt-2 my-lg-auto">
				<h4 class="mx-auto mb-0"><?php echo $sub_3_title; ?></h4>
				<p class="mx-auto"><?php echo $sub_3_description; ?></p>
				<ul class="subscription-card--list">
					<li class="plus"><p><?php echo $sub_3_usp_icon_1 . $sub_3_usp_1; ?></p></li>
					<li class="plus"><p><?php echo $sub_3_usp_icon_2 . $sub_3_usp_2; ?></p></li>
					<li class="plus"><p><?php echo $sub_3_usp_icon_3 . $sub_3_usp_3; ?></p></li>
				</ul>
				<div class="my-3 mx-auto">
					<a href="<?php echo home_url(); ?>/abonnement/" class="btn-orange-outlined"><?php echo $sub_3_button; ?><i class="bstz bstz-double-arrow"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>