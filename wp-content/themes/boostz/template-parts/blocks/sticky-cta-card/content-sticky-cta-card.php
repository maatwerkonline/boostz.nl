<?php
/**
 * Block Name: Sticky CTA Card
 * This is the template that displays the block sticky-cta-card.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$image = get_field('image');
$image_url = wp_get_attachment_image_src( $image, 'medium');
$title = get_field('title');
$text = get_field('text');
$button_title = get_field('button_title');
$button_url = get_field('button_url');
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> sticky_cta"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<div class="sticky_cta__image">
		<img src="<?php echo $image_url[0]; ?>">
	</div>	
	<div class="sticky_cta__title">
		<h4>
			<?php echo $title; ?>
		</h4>
	</div>
	<div class="sticky_cta__content">
		<?php echo $text; ?>
	</div>	
	<a href="<?php echo $button_url; ?>" class="btn-orange-outlined sticky_cta__btn">
		<?php echo $button_title ?>
		<i class="bstz bstz-double-arrow"></i>
	</a>
</div>