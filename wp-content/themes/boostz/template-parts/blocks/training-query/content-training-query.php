<?php
/**
 * Block Name: Training query
 * This is the template that displays the block training-query.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- training query -->
	<?php
	// Get all the categories for the courses
	$args = array(
				'taxonomy' => 'course-category',
				'posts_per_page' => -1
			);

	$cats = get_categories($args);
	?>

	<div class="cat-filter d-flex pb-3">
		<span class="active" value="alles"><?php _e('Alles', 'maatwerkonline') ?></span>
		<?php
		foreach($cats as $cat) {
			?>
			<span value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></span>
			<?php
		}
		?>
	</div>

	<?php
	// Reset query
	wp_reset_query();

	// If filter is not set use this args
	$args = array(
		'post_type' => 'course',
		'post_status' => 'publish',
		'posts_per_page' => -1
	);

	$query = new WP_Query($args);

	?>
	<div class="ajax-response position-relative">
		<?php
			include('training-filter-content.php');
		?>
	</div>
</div>