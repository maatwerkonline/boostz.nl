<div class="spinner-pos d-none">
	<div class="html-spinner"></div>
</div>
<div class="training-slider">
		<?php
	
		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();
			// Get the teachers image
			$teacher_id = get_the_author_ID(get_the_ID());
			$teacher_image = mt_profile_img($teacher_id, array('size' => 'small', 'echo' => false ));
			// Get the teachers name
			$teacher_name = get_author_name($teacher_id);
			// Calculate the total amount of lessons in the course
			$lesson_array = get_post_meta(get_the_ID(), '_lesson_order');
			$clean_array = explode(',', $lesson_array[0]);
			$lesson_count = count($clean_array);
			// Get the course information data
			$course_type = get_post_meta(get_the_ID(  ), 'course_type');
			$duration = get_post_meta(get_the_ID(  ), 'course_duration');
			?>
			<div class="pr-3">
				<div class="training-card">
					<a href="<?php echo get_permalink(); ?>">
						<div class="position-relative">
							<div class="training-card--play-button"></div>
							<div class="training-card--course-type">
								<p class="mb-0"><?php echo $course_type[0]; ?></p>
							</div>
							<img class="training-card--image" data-lazy="<?php echo get_the_post_thumbnail_url(); ?>">
						</div>
						<div class="p-3">
							<h6><?php echo get_the_title(); ?></h6>
							<div class="training-card--bottom d-flex">
								<?php echo $teacher_image; ?>
								<p class="my-auto"><?php echo $teacher_name; ?></p>
								<div class="ml-auto">
									<p class="mb-0 text-right"><?php echo $duration[0]; ?> min <i class="bstz bstz-time"></i></p>
									<?php 
										if($lesson_count > 1):
										echo '<p class="mb-0 text-right">' . $lesson_count . ' Lessen <i class="bstz bstz-camera"></i></p>';
										else:
											echo '<p class="mb-0 text-right">' . $lesson_count . ' Les <i class="bstz bstz-camera"></i></p>';
										endif;
									?>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<?php
		}
		// Reset query
		wp_reset_query();
		?>
	</div>