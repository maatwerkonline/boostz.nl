jQuery(document).ready(function ($) {
    "use strict";

    $('.training-slider').not('.slick-initialized').slick({
        infinite: false,
        lazyLoad: 'progressive',
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        prevArrow: "<i class='bstz bstz-double-arrow-back prev' aria-hidden='true'></i>",
        nextArrow: "<i class='bstz bstz-double-arrow next' aria-hidden='true'></i>",
        dots: true,
        responsive: [
            {
                breakpoint: 772,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });
    $(".training-slider .slick-dots").wrap("<div class='slick-dots-container d-flex justify-content-center'></div>")
    $(".training-slider .slick-dots-container").prepend($(".training-slider i.prev"));
    $(".training-slider .slick-dots-container").append($(".training-slider i.next"));

    $('.cat-filter span').on('click', function () {
        var category = $(this).attr('value');
        // console.log(category);

        $('.cat-filter span.active').removeClass('active');
        $(this).addClass('active');

        $.ajax({ // you can also use $.post here
            url: objectL10n.ajaxurl, // AJAX handler
            data: {
                'action': 'training_filter',
                'category': category,
            },
            type: 'POST',
            beforeSend: function (xhr) {
                $('.ajax-response .slick-slide').each(function () {
                    $(this).addClass('ajax-fade');
                })
                $('.ajax-response .slick-dots, .cat-filter').addClass('ajax-fade');
                $('.spinner-pos').removeClass('d-none');
            },
            success: function (data) {
                $('.ajax-response .slick-slide').each(function () {
                    $(this).removeClass('ajax-fade');
                })
                $('.ajax-response .slick-dots, .cat-filter').removeClass('ajax-fade');
                $('.spinner-pos').addClass('d-none');
                if (data) {
                    $('.ajax-response').html(data);
                }

                $('.training-slider').slick({
                    infinite: false,
                    lazyLoad: 'progressive',
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: true,
                    prevArrow: "<i class='bstz bstz-double-arrow-back prev' aria-hidden='true'></i>",
                    nextArrow: "<i class='bstz bstz-double-arrow next' aria-hidden='true'></i>",
                    dots: true,
                    responsive: [
                        {
                            breakpoint: 772,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

                $(".training-slider .slick-dots").wrap("<div class='slick-dots-container d-flex justify-content-center'></div>")
                $(".training-slider .slick-dots-container").prepend($(".training-slider i.prev"));
                $(".training-slider .slick-dots-container").append($(".training-slider i.next"));



            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.ajax-response').html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            },
        });
    });

});