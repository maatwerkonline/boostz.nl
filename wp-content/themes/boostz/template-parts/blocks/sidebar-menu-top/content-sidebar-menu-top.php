<?php
/**
 * Block Name: Sidebar Menu Top
 * This is the template that displays the block sidebar-menu-top.
 *
 * @author admin
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

?>
<?php wp_nav_menu( array( 'menu' => 'mobile', 'theme_location' => 'mobile', 'container_id' => 'sidebar-wrapper', 'container_class' => 'border-right', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
