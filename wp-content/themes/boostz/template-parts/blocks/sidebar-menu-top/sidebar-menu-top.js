jQuery(document).ready(function ($) {
    $('.content-container').on('click', function() {
        if($('.wrapper').hasClass('toggled')) {
            $('body').toggleClass('menu-open');
            $('#menu-toggle').removeClass('active');
            $('.wrapper').removeClass('toggled');
        };
    }).on('click', '#menu-toggle', function(e) {
        e.stopPropagation();
        if($('#menu-toggle').hasClass('inactive')) {
            $('body').toggleClass('menu-open');
            parent.history.back();
            return false;
        } else {
            $('body').toggleClass('menu-open');
            $(this).toggleClass('active');
            $('.wrapper').toggleClass('toggled');
        }
        $(window).scrollTop(0);
    });
});