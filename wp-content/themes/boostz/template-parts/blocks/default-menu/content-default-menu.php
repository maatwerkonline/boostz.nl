<?php
/**
 * Block Name: Default Menu
 * This is the template that displays the block default-menu.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */


?>
 
 <nav class="navbar navbar-main" role="navigation">
	<div class="container">
		<?php if (!is_checkout() ) : ?>
			<?php wp_nav_menu( array( 'menu' => 'default', 'theme_location' => 'default', 'container_id' => 'default-menu', 'container_class' => 'd-none d-lg-block w-100', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
		<?php endif; ?>
		<div id="menu-toggle" class="d-block d-lg-none <?php if(is_page('offline')){ ?> inactive <?php }; ?>">
			<div class="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</div>
		<div class="clear">
		</div>
	</div>
</nav>