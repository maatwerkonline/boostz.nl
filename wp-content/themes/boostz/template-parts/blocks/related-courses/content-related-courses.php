<?php
/**
 * Block Name: Related Courses
 * This is the template that displays the block related-courses.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$related_courses = get_field('related_courses');

?>
 
 <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> training-query"> <!-- training query -->
	<?php
	// If filter is not set use this args
	$args = array(
		'post_type' => 'course',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'post__in'      => $related_courses
	);

	$query = new WP_Query($args);

	?>
	<div class="ajax-response position-relative">
		<?php
			include('training-filter-content.php');
		?>
	</div>
</div>