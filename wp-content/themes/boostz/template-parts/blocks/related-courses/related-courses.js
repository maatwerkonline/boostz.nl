(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockrelatedcourses = function() {
		jQuery(document).ready(function ($) {
			"use strict";
		
			$('.training-slider').slick({
				infinite: false,
				lazyLoad: 'progressive',
				slidesToShow: 3,
				slidesToScroll: 3,
				arrows: true,
				prevArrow: "<i class='bstz bstz-double-arrow-back prev' aria-hidden='true'></i>",
				nextArrow: "<i class='bstz bstz-double-arrow next' aria-hidden='true'></i>",
				dots: true,
				responsive: [
					{
						breakpoint: 772,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			});
		
			$(".training-slider .slick-dots").prepend($(".training-slider i.prev"));
			$(".training-slider .slick-dots").append($(".training-slider i.next"));
		
			$('.cat-filter span').on('click', function () {
				var category = $(this).attr('value');
				// console.log(category);
		
				$('.cat-filter span.active').removeClass('active');
				$(this).addClass('active');
		
				$.ajax({ // you can also use $.post here
					url: objectL10n.ajaxurl, // AJAX handler
					data: {
						'action': 'training_filter',
						'category': category,
					},
					type: 'POST',
					beforeSend: function (xhr) {
						$('.ajax-response .slick-slide').each(function () {
							$(this).addClass('ajax-fade');
						})
						$('.ajax-response .slick-dots, .cat-filter').addClass('ajax-fade');
						$('.spinner-pos').removeClass('d-none');
					},
					success: function (data) {
						$('.ajax-response .slick-slide').each(function () {
							$(this).removeClass('ajax-fade');
						})
						$('.ajax-response .slick-dots, .cat-filter').removeClass('ajax-fade');
						$('.spinner-pos').addClass('d-none');
						if (data) {
							$('.ajax-response').html(data);
						}
		
						$('.training-slider').slick({
							infinite: false,
							lazyLoad: 'progressive',
							slidesToShow: 3,
							slidesToScroll: 3,
							arrows: true,
							prevArrow: "<i class='bstz bstz-double-arrow-back prev' aria-hidden='true'></i>",
							nextArrow: "<i class='bstz bstz-double-arrow next' aria-hidden='true'></i>",
							dots: true,
							responsive: [
								{
									breakpoint: 772,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1
									}
								}
							]
						});
						$(".training-slider .slick-dots").prepend($(".training-slider i.prev"));
						$(".training-slider .slick-dots").append($(".training-slider i.next"));
		
		
		
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$('.ajax-response').html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
					},
				});
			});
		
		});
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=related-courses', initializeBlockrelatedcourses );
	} else {
		$(document).ready(function(){
			initializeBlockrelatedcourses();
		});
	}
})(jQuery);
				