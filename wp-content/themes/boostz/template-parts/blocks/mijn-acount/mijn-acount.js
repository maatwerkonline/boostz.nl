(function ($) {
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlockmijnacount = function () {

	}

	// Initialize dynamic block preview (editor).
	if (window.acf) {
		window.acf.addAction('render_block_preview/type=mijn-acount', initializeBlockmijnacount);
	} else {
		$(document).ready(function () {
			initializeBlockmijnacount();
		});
	}

	$(".tab-title").on('click', function () {
		// remove classes from all
		$(".tab-title").removeClass("active");
		// add class to the one we clicked
		$(this).addClass("active");
	});
})(jQuery);
