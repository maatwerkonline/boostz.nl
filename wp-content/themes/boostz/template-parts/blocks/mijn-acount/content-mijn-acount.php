<?php
/**
 * Block Name: mijn-acount
 * This is the template that displays the block mijn-acount.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Mijn account blok -->
	<?php if (!is_user_logged_in()){ ?>
		<div class="row">
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-6 d-flex">
				<div class="mx-auto">
					<?php echo do_shortcode('[gravityform action="login" title="false" description="false" logged_in_message="Geslaagd, je bent nu ingelogd" registration_link_text="" forgot_password_text="Wachtwoord vergeten?" /]'); ?>
				</div>
			</div>
			<div class="col-12 col-md-3"></div>
		</div>
	<?php } else { ?>
		<?php
		$form_id_1 = get_field('formulier_id_1');
		$form_id_2 = get_field('formulier_id_2');

		$form_title_1 = get_field('formulier_title_1');
		$form_title_2 = get_field('formulier_title_2');
		?>
		<div class="row">
			<div class="col-12 col-md-6 pr-3">
				<h2 class="pb-3"><?php echo $form_title_1; ?></h2>
				<?php gravity_form( $form_id_1, false, false, false, '', true, 12 ); ?>
			</div>
			<div class="col-12 col-md-6 pr-3 pt-5 pt-md-0">
				<h2 class="pb-3"><?php echo $form_title_2; ?></h2>
				<?php gravity_form( $form_id_2, false, false, false, '', true, 12 ); ?>
			</div>
		</div>
	<?php } ?>
</div>