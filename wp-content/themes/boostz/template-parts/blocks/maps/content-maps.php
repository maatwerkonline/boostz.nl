<?php
/**
 * Block Name: Maps
 * This is the template that displays the block maps.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php
	
	?>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2434.656885187095!2d4.8441408157937165!3d52.39477007979072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c5e2f4ad4bd389%3A0x9b3111bf3283cc1e!2sBoostz!5e0!3m2!1snl!2sfr!4v1637665538679!5m2!1snl!2sfr" width="100%" height="auto" style="border:0; aspect-ratio: 16/9;" allowfullscreen="" loading="lazy"></iframe>
</div>