<?php
/**
 * Block Name: Course top teacher information
 * This is the template that displays the block course-top-teacher-information.
 *
 * @author maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

if(!empty($_GET['post'])) {
	$course = $_GET['post'];
} else {
	$course = get_the_id();
}

$teacher_id = get_the_author_ID($course);
$teacher_image = mt_profile_img($teacher_id, array('size' => 'small', 'echo' => false ));
// Get the teachers name
$teacher_name = get_author_name($teacher_id);
// Get the teachers functions
$teacher_functions = nl2br(get_the_author_meta( 'teacher_functions', $teacher_id ));


?>
 
<div class="d-flex teacher">
	<?php echo $teacher_image;?>
	<p class="my-auto pl-3 text-capitalize">
		<?php echo $teacher_name . ', ' . $teacher_functions; ?>
	</p>
</div>