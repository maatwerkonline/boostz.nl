<?php
$author_id = get_the_author_ID(get_the_id());
$author_image = mt_profile_img($author_id, array('size' => 'small', 'echo' => false ));
$author = get_author_name($author_id);
$categories = wp_get_post_categories(get_the_id());
?>
<div class="col-md-6 col-12 blog_article mb-4">
    <a href="<?php echo get_permalink(); ?>" class="blog_article__content_holder">
        <div class="blog_article__content_holder__image">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_id(), 'medium'); ?>">
            <?php if($categories) : ?>
                <div class="blog_article__content_holder__image__categories">
                    <?php foreach($categories as $category) :
                        $cat = get_category( $category );
                        $cat_id = get_cat_ID( $cat->name );
                        $cat_ids[] = get_cat_ID( $cat->name );
                        ?>
                        <span href="<?php echo get_site_url(); ?>/boostz/blog/?term=<?php echo $cat_id; ?>" class="mr-2 mb-2"><?php echo $cat->name; ?></span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="blog_article__content_holder__author_date d-flex p-3">
            <?php echo $author_image; ?>
            <p class="my-auto"><?php echo $author; ?></p>
            <div class="ml-auto my-auto">
                <p class="mb-0 text-right"><?php echo get_the_date( 'm-d-Y' ) ?> <i class="bstz bstz-time"></i></p>							
            </div>
        </div>
        <h6 class="blog_article__content_holder__title pl-3 pr-3 pt-3">
            <?php
                echo get_the_title();
            ?>
        </h6>
        <div class="blog_article__content_holder__content pl-3 pr-3 pb-2">
            <p>
                <?php
                    echo get_the_excerpt();
                ?>
            </p>
        </div> 
    </a>   
</div>