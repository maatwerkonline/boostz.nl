<?php

/**
 * Snippet Name: Hooks related to Sensei
 * Description: All hooks related to Sensei
 * Version: 1.0
 * Author: Frank van 't Hof
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

remove_action('sensei_single_lesson_content_inside_after', 'sensei_the_single_lesson_meta', 10);
remove_action('sensei_pagination', array(Sensei()->frontend, 'sensei_breadcrumb'), 80, 1);
remove_action('sensei_course_content_inside_before', array('Sensei_Templates', 'the_title'), 5);
