

<?php

/**
 * Snippet Name: Only show administrators admin bar
 * Description: This function makes sure that only administrators will be able to see the admin bar
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function is_current_user_administrator() {
    global $current_user;

    return in_array( 'administrator', $current_user->roles );
}
add_filter( 'show_admin_bar', 'is_current_user_administrator' );