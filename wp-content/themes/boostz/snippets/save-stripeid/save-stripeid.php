<?php

/**
 * Snippet Name: Save Stripe customer ID to user meta data
 * Description: Use this function to save the Stripe customer ID to the user meta of the customer
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action( 'gform_stripe_customer_after_create', 'save_stripe_customer_id' );
function save_stripe_customer_id( $customer ) {
    if ( is_user_logged_in () ) {
        update_user_meta( get_current_user_id(), '_stripe_customer_id', $customer->id );
    }
}