<?php

/**
 * Snippet Name: Add the function course_lessons() to your site
 * Description: This function makes it possible to fetch all lessons related to a course in an array. This function accepts two arguments: course_lessons( $course_id, $post_status )  
 * Version: 1.0
 * Author: Frank van 't Hof
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function course_lessons( $course_id, $post_status, $fields = 'all', $query_args = [] ) {

	if ( is_a( $course_id, 'WP_Post' ) ) {
		$course_id = $course_id->ID;
	}

	$query_args = array_merge(
		$query_args,
		[
			'post_type'        => 'lesson',
			'posts_per_page'   => -1,
			'orderby'          => 'date',
			'order'            => 'ASC',
			'post_status'      => $post_status,
			'suppress_filters' => 0,
		]
	);

	if ( ! isset( $query_args['meta_query'] ) ) {
		$query_args['meta_query'] = [];
	}

	$query_args['meta_query'][] = [
		'key'   => '_lesson_course',
		'value' => intval( $course_id ),
	];

	$query_results = new WP_Query( $query_args );
	$lessons       = $query_results->posts;

	// re order the lessons. This could not be done via the OR meta query as there may be lessons
	// with the course order for a different course and this should not be included. It could also not
	// be done via the AND meta query as it excludes lesson that does not have the _order_$course_id but
	// that have been added to the course.

	/**
	 * Filter runs inside Sensei_Course::course_lessons function
	 *
	 * Returns all lessons for a given course
	 *
	 * @param array $lessons
	 * @param int $course_id
	 */
	$lessons = apply_filters( 'sensei_course_get_lessons', $lessons, $course_id );

	// return the requested fields
	// runs after the sensei_course_get_lessons filter so the filter always give an array of lesson
	// objects
	if ( 'ids' === $fields ) {
		$lesson_objects = $lessons;
		$lessons        = array();

		foreach ( $lesson_objects as $lesson ) {
			$lessons[] = $lesson->ID;
		}
        uasort( $lessons, _short_course_lessons_callback());
	}

	return $lessons;

}

function _short_course_lessons_callback( $lesson_1, $lesson_2 ) {

    if ( $lesson_1->course_order == $lesson_2->course_order ) {
        return 0;
    }

    return ( $lesson_1->course_order < $lesson_2->course_order ) ? -1 : 1;
}

function sensei_lesson_video( $post_id = 0 ) {
	if ( 0 < intval( $post_id ) && sensei_can_user_view_lesson( $post_id ) ) {
		$lesson_video_embed = get_post_meta( $post_id, '_lesson_video_embed', true );
		if ( 'http' == substr( $lesson_video_embed, 0, 4 ) ) {
			// V2 - make width and height a setting for video embed.
			$lesson_video_embed = wp_oembed_get( esc_url( $lesson_video_embed ) );
		}

		$lesson_video_embed = do_shortcode( html_entity_decode( $lesson_video_embed ) );
		$lesson_video_embed = Sensei_Wp_Kses::maybe_sanitize( $lesson_video_embed, 1 );

		if ( '' != $lesson_video_embed ) {
			?>
			<div class="video"><?php echo wp_kses( $lesson_video_embed, 1 ); ?></div>
			<?php
		}
	}
}