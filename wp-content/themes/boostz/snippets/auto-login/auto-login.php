<?php

/**
 * Snippet Name: Function auto login after user registered
 * Description: Use this function to make users login automatically after registering with Gravity Forms.
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action("gform_user_registered", "autologin", 10, 4);
function autologin($user_id, $config, $entry, $password) {
        wp_set_auth_cookie($user_id, false, '');
}