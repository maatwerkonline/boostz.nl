<?php

/**
 * Snippet Name: Redirect user after login
 * Description: Use this function to redirect the user to the homepage after logging in
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action('wp_login','auto_redirect_after_login');
function auto_redirect_after_login(){
  wp_safe_redirect( home_url() );
  exit;
}