<?php

/**
 * Snippet Name: Move Gravity Forms currency icon
 * Description: Use this function to place the currency icon within Gravity Forms on the right side of the price.
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_filter( 'gform_currencies', 'gw_modify_currencies' );
function gw_modify_currencies( $currencies ) {

 $currencies['EUR'] = array(
 'name' => esc_html__( 'Euro', 'gravityforms' ),
 'symbol_left' => '&#8364;',
 'symbol_right' => '',
 'symbol_padding' => ' ',
 'thousand_separator' => '.',
 'decimal_separator' => ',',
 'decimals' => 2
 );

 return $currencies;
}