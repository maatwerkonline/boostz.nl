<?php

/**
 * Snippet Name: Change username on login form
 * Description: change the username to whatever you would like on the Gravity Forms login form
 * Version: 1.0
 * Author: Matthijs Vis
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_filter( 'gform_userregistration_login_form', 'change_form', 10, 1 );
function change_form( $form ) {
    $fields = $form['fields'];
    foreach ( $fields as &$field ) {
        if ( $field->label == 'Gebruikersnaam' ) {
            $field->label = 'Email';
        }
    }
    return $form;
}