<?php
global $acf_icons_array;
$acf_icons_array['bstz'] = array(
'name' => 'Boostz Icons',
'icons' => array(
 'bstz-youtube' => '<i class="bstz bstz-youtube"></i> bstz-youtube',
 'bstz-twitter' => '<i class="bstz bstz-twitter"></i> bstz-twitter',
 'bstz-linkedin' => '<i class="bstz bstz-linkedin"></i> bstz-linkedin',
 'bstz-facebook' => '<i class="bstz bstz-facebook"></i> bstz-facebook',
 'bstz-double-arrow' => '<i class="bstz bstz-double-arrow"></i> bstz-double-arrow',
 'bstz-double-arrow-back' => '<i class="bstz bstz-double-arrow-back"></i> bstz-double-arrow-back',
 'bstz-time' => '<i class="bstz bstz-time"></i> bstz-time',
 'bstz-camera' => '<i class="bstz bstz-carmera"></i> bstz-camera',
 'bstz-check' => '<i class="bstz bstz-check"></i> bstz-check',
 'bstz-minus' => '<i class="bstz bstz-minus"></i> bstz-minus',
 'bstz-home' => '<i class="bstz bstz-home"></i> bstz-home',
 'bstz-person' => '<i class="bstz bstz-person"></i> bstz-person'
)
);