<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
				</div>
				<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-6 col-md-3"><?php dynamic_sidebar( __('First Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-6 col-md-3"><?php dynamic_sidebar( __('Second Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-12 col-md-3"><?php dynamic_sidebar( __('Third Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-12 col-md-3 pt-3 pt-md-0"><?php dynamic_sidebar( __('Fourth Footer Widget', 'onm_textdomain') );?></div>
						</div>
					</div>
				</footer>
				<div class="container">
					<div class="row">
						<div class="footer-trademark col-12 d-flex text-white py-2 footer-nav">
							<p class="mx-auto m-0">Website door <a href="https://www.maatwerkonline.nl/" target="_blank"><strong><u>Maatwerk Online</u></strong></a></p>
						</div>
					</div>
				</div>
			</div> <!-- end #page-content-wrapper -->

		</div> <!-- end #wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
