<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php if ( !is_front_page( ) ) :
	// get reusable gutenberg block:
	$gblock = get_post( 1247 );
	echo apply_filters( 'the_content', $gblock->post_content );
endif;
?>

<div class="wp-block-group p-0">
	<div class="row">
		<div class="col-12 col-md-1"></div>
		<div class="col-12 col-md-11">
			<h3><?php echo get_the_title(); ?></h3>
		</div>
	</div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	<?php the_content();?>
<?php endwhile; endif;?>

<?php get_footer();