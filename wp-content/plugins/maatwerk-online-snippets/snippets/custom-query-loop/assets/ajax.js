function loadloopresults(post_type, postperpage, taxonomy, term_id, current, pagination_url, template_part, posts_total, row, orderby, order, termfilter, taxonomyfilter, loader ) {
    jQuery(document).ready(function($){
        var data = {
            action: 'getpostsloop',
            postType: post_type,
            postperpage:  postperpage,
            taxonomy: taxonomy,
            termID: term_id,
            current: current,
            paginationURL: pagination_url,
            templatePart: template_part,
            postsTotal: posts_total,
            row: row,
            orderby: orderby,
            order: order,
            termfilter: termfilter,
            taxonomyfilter: taxonomyfilter,
            loader: loader,
        };
        // Ajax call
        $.ajax({
            type: "post",
            url: objectL10n.ajaxurl,
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $(".container-"+pagination_url).html(data);
                $('.loop-container').removeClass('loading');
            },
            'error': function (data) {
                alert('something went wrong');
            }
        });
    });
}
