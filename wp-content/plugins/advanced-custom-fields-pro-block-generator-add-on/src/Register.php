<?php
namespace MOB;

abstract class Register {
	public function __construct() {
		add_action( 'init', [ $this, 'register' ], 5 );
		add_filter( 'post_updated_messages', [ $this, 'updated_message' ] );
		add_filter( 'bulk_post_updated_messages', [ $this, 'bulk_updated_messages' ], 10, 2 );
		add_action( 'publish_mo-block', [ $this, 'blocksfile' ], 10, 2 );
		add_action( 'add_meta_boxes', [ $this, 'block_metaboxes' ] );
		add_action( 'post_updated', [ $this, 'check_for_block_title_change' ], 10, 3 );
		add_action( 'admin_init', [ $this, 'remove_block_file' ] );
		add_action( 'admin_notices', [ $this, 'block_admin_notice' ] );
		add_action( 'admin_head', [ $this, 'my_admin_css' ] );
		add_action( 'delete_post', [ $this, 'block_delete' ], 10 );
		add_filter( 'bulk_actions-edit-mo-block', [ $this, 'block_bulk_delete' ], 10, 3 );
		add_action( 'save_post', [ $this, 'check_blockexist_callback' ], 10, 2 );
	}

	// Migration helper methods.
	protected function unarray( &$value, $key, $ignore = [] ) {
		$value = 1 === count( $value ) && ! in_array( $key, $ignore, true ) ? $value[0] : $value;
	}

	protected function normalize_checkbox( &$value ) {
		if ( is_numeric( $value ) && in_array( $value, [0, 1] ) ) {
			$value = 1 == (int) $value;
		}
	}

	protected function change_key( &$array, $from, $to ) {
		if ( isset( $array[ $from ] ) ) {
			$array[ $to ] = $array[ $from ];
		}
		unset( $array[ $from ] );
	}
}
