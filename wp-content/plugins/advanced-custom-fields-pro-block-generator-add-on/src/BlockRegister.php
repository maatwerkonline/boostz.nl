<?php
namespace MOB;

use WP_Post;

session_start();

class BlockRegister extends Register {
	public function register() {
		// Register main post type 'mo-block'.
		$labels = array(
			'name'               => _x( 'Blocks', 'Post Type General Name', 'acf-block-generator' ),
			'singular_name'      => _x( 'Block', 'Post Type Singular Name', 'acf-block-generator' ),
			'menu_name'          => __( 'Blocks', 'acf-block-generator' ),
			'name_admin_bar'     => __( 'Block', 'acf-block-generator' ),
			'all_items'          => __( 'Blocks', 'acf-block-generator' ),
			'add_new_item'       => __( 'Add New Block', 'acf-block-generator' ),
			'add_new'            => __( 'New Block', 'acf-block-generator' ),
			'new_item'           => __( 'New Block', 'acf-block-generator' ),
			'edit_item'          => __( 'Edit Block', 'acf-block-generator' ),
			'update_item'        => __( 'Update Block', 'acf-block-generator' ),
			'view_item'          => __( 'View Block', 'acf-block-generator' ),
			'search_items'       => __( 'Search Block', 'acf-block-generator' ),
			'not_found'          => __( 'Not found', 'acf-block-generator' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'acf-block-generator' ),
		);
		$args   = array(
			'label'         => __( 'Blocks', 'acf-block-generator' ),
			'labels'        => $labels,
			'supports'      => false,
			'public'        => false,
			'show_ui'       => true,
			'show_in_menu'  => defined( 'RWMO_VER' ) ? 'meta-box' : null,
			'menu_icon'     => 'dashicons-block-default',
			'can_export'    => false,
			'rewrite'       => false,
			'query_var'     => false,
			'menu_position' => 80,
		);
		register_post_type( 'mo-block', $args );

		// Get all registered blocks.
		$blocks = $this->get_blocks();
		if( function_exists('acf_register_block') ) {
			foreach ( $blocks as $post_type => $args ) {
				// acf_register_block_type for each registered block
				acf_register_block_type( $args );
			}
		}
	}

	public function get_blocks() {
		$blocks = [];

		$posts = get_posts( [
			'posts_per_page'         => -1,
			'post_status'            => 'publish',
			'post_type'              => 'mo-block',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );

		foreach ( $posts as $post ) {
			$data = $this->get_block_data( $post );
			$blocks[ $data['name'] ] = $data;
		}

		wp_reset_postdata();

		return $blocks;
	}

	public function get_block_data( WP_Post $post ) {
		return empty( $post->post_content ) || isset( $_GET['mob-force'] ) ? $this->migrate_data( $post ) : json_decode( $post->post_content, true );
	}

	private function migrate_data( WP_Post $post ) {
		$args      = [ 'labels' => [] ];
		$post_meta = get_post_meta( $post->ID );

		foreach ( $post_meta as $key => $value ) {
			if ( 0 !== strpos( $key, 'label_' ) && 0 !== strpos( $key, 'args_' ) ) {
				continue;
			}
			$this->unarray( $value, $key, [ 'args_post_types', 'args_supports' ] );
			$this->normalize_checkbox( $value );
			$value = 'args_menu_position' === $key ? (int) $value : $value;

			if ( 0 === strpos( $key, 'label_' ) ) {
				$key = str_replace( 'label_', '', $key );
				$args['labels'][ $key ] = $value;
			} else {
				$key = str_replace( 'args_', '', $key );
				$args[ $key ] = $value;
			}
		}
		$this->change_key( $args, 'post_type', 'name' );

		// Bypass new post types.
		if ( isset( $_GET['mob-force'] ) && empty( $args['name'] ) ) {
			return json_decode( $post->post_content, true );
		}

		wp_update_post( [
			'ID'           => $post->ID,
			'post_content' => wp_json_encode( $args, JSON_UNESCAPED_UNICODE ),
		] );

		return $args;
	}

	public function updated_message( $messages ) {
		$post             = get_post();
		$post_type_object = get_post_type_object( $post->post_type );
		$label            = ucfirst( $post_type_object->labels->singular_name );
		$label_lower      = strtolower( $label );
		$label            = ucfirst( $label_lower );
		$revision         = filter_input( INPUT_GET, 'revision', FILTER_SANITIZE_NUMBER_INT );
		wp_reset_postdata();

		$message = [
			0  => '', // Unused. Messages start at index 1.
			// translators: %s: Name of the custom post type in singular form.
			1  => sprintf( __( '%s updated.', 'acf-block-generator' ), $label ),
			2  => __( 'Custom field updated.', 'acf-block-generator' ),
			3  => __( 'Custom field deleted.', 'acf-block-generator' ),
			// translators: %s: Name of the custom post type in singular form.
			4  => sprintf( __( '%s updated.', 'acf-block-generator' ), $label ),
			// translators: %1$s: Name of the custom post type in singular form, %2$s: Revision title.
			5  => $revision ? sprintf( __( '%1$s restored to revision from %2$s.', 'acf-block-generator' ), $label, wp_post_revision_title( $revision, false ) ) : false,
			// translators: %s: Name of the custom post type in singular form.
			6  => sprintf( __( '%s published.', 'acf-block-generator' ), $label ),
			// translators: %s: Name of the custom post type in singular form.
			7  => sprintf( __( '%s saved.', 'acf-block-generator' ), $label ),
			// translators: %s: Name of the custom post type in singular form.
			8  => sprintf( __( '%s submitted.', 'acf-block-generator' ), $label ),
			// translators: %1$s: Name of the custom post type in singular form, %2$s: Revision title.
			9  => sprintf( __( '%1$s scheduled for: <strong>%2$s</strong>.', 'acf-block-generator' ), $label, date_i18n( __( 'M j, Y @ G:i', 'acf-block-generator' ), strtotime( $post->post_date ) ) ),
			// translators: %s: Name of the custom post type in singular form.
			10 => sprintf( __( '%s draft updated.', 'acf-block-generator' ), $label ),
		];

		// Get all post where where post_type = mo-block.
		$blocks = get_posts( [
			'posts_per_page'         => -1,
			'post_status'            => 'any',
			'post_type'              => 'mo-block',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
		$data = '';
		foreach ( $blocks as $post_type ) {
			$data = $this->get_block_data( $post_type );
			if(!empty($data)){
				$slug = $data['name'];
			}
			$messages[ $slug ] = $message;

			if ( empty( $data['publicly_queryable'] ) ) {
				continue;
			}

			$permalink = get_permalink( $post->ID );

			// translators: %s: Post link, %s: View post text, %s: Post type label.
			$view_link             = sprintf( ' <a href="%s">%s</a>.', esc_url( $permalink ), sprintf( __( 'View %s', 'acf-block-generator' ), $label_lower ) );
			$messages[ $slug ][1] .= $view_link;
			$messages[ $slug ][6] .= $view_link;
			$messages[ $slug ][9] .= $view_link;

			$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
			// translators: %s: Post link, %s: Preview post text, %s: Post type label.
			$preview_link           = sprintf( ' <a target="_blank" href="%s">%s</a>.', esc_url( $preview_permalink ), sprintf( __( 'Preview %s', 'acf-block-generator' ), $label_lower ) );
			$messages[ $slug ][8]  .= $preview_link;
			$messages[ $slug ][10] .= $preview_link;
		}

		$messages['mo-block'] = $message;

		wp_reset_postdata();

		return $messages;
	}

	public function bulk_updated_messages( $bulk_messages, $bulk_counts ) {
		$labels = [
			'mo-block' => [
				'singular' => __( 'block', 'acf-block-generator' ),
				'plural'   => __( 'blocks', 'acf-block-generator' ),
			],
		];

		// Get all post where where post_type = mo-block.
		$blocks = get_posts( [
			'posts_per_page'         => -1,
			'post_status'            => 'any',
			'post_type'              => 'mo-block',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
		foreach ( $blocks as $post_type ) {
			$data = $this->get_block_data( $post_type );
			$slug = $data['name'];

			$labels[ $slug ] = [
				'singular' => strtolower( $data['title'] ),
				'plural'   => strtolower( $data['title'] ),
			];
		}

		foreach ( $labels as $post_type => $label ) {
			$singular = $label['singular'];
			$plural   = $label['plural'];

			$bulk_messages[ $post_type ] = array(
				// translators: %1$s: Number of items, %2$s: Name of the post type in singular or plural form.
				'updated'   => sprintf( __( '%1$s %2$s updated.', 'acf-block-generator' ), $bulk_counts['updated'], $bulk_counts['updated'] > 1 ? $plural : $singular ),
				// translators: %1$s: Number of items, %2$s: Name of the post type in singular or plural form.
				'locked'    => sprintf( __( '%1$s %2$s not updated, somebody is editing.', 'acf-block-generator' ), $bulk_counts['locked'], $bulk_counts['locked'] > 1 ? $plural : $singular ),
				// translators: %1$s: Number of items, %2$s: Name of the post type in singular or plural form.
				'deleted'   => sprintf( __( '%1$s %2$s permanently deleted.', 'acf-block-generator' ), $bulk_counts['deleted'], $bulk_counts['deleted'] > 1 ? $plural : $singular ),
				// translators: %1$s: Number of items, %2$s: Name of the post type in singular or plural form.
				'trashed'   => sprintf( __( '%1$s %2$s moved to the Trash.', 'acf-block-generator' ), $bulk_counts['trashed'], $bulk_counts['trashed'] > 1 ? $plural : $singular ),
				// translators: %1$s: Number of items, %2$s: Name of the post type in singular or plural form.
				'untrashed' => sprintf( __( '%1$s %2$s restored from the Trash.', 'acf-block-generator' ), $bulk_counts['untrashed'], $bulk_counts['untrashed'] > 1 ? $plural : $singular ),
			);
		}

		wp_reset_postdata();

		return $bulk_messages;
	}

	// Publish hook add folder files
	public function blocksfile($ID, $post ) {

		$post_content = json_decode($post->post_content);
		$enqueue_style = $post_content->enqueue_style;
		$enqueue_script = $post_content->enqueue_script;
		$user_info = get_userdata($post->post_author);
		$output_dir = get_theme_file_path("/template-parts/blocks/");
	    $folder_name = $post_content->name;

	    if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $folder_name)) {
		   $folder_name = 	preg_replace('/[^A-Za-z0-9\-]/', '-', $folder_name);
		} else {
			$folder_name = $post_content->name;
		};

		$blockName =  get_post_meta($ID, 'block_name', $folder_name);

		if(!empty($blockName)){	
		    if($blockName != $folder_name){
		    	$filename = '/content-'.$blockName;
		    	$oldFileName = $output_dir . $blockName . $filename.".php";
		    	if(file_exists($oldFileName)){
		    		unlink($oldFileName);
		    	}

		    	$jsfile ="/".$blockName;
		    	$oldFilescript = $output_dir . $blockName . $jsfile.".js";
		    	if(file_exists($oldFilescript)){
		    		unlink($oldFilescript);
		    	}

		    	$cssfile ="/".$blockName;
		    	$oldFilestyle = $output_dir . $blockName . $cssfile.".css";
		    	
		    	if(file_exists($oldFilestyle)){
		    		unlink($oldFilestyle);
		    	}

		    	$oldfolder = $output_dir . $blockName;
		    	if(file_exists($oldfolder)){	    	
		    			@rmdir($oldfolder);
		    	}
		    }
		}

		update_post_meta($ID, 'block_name', $folder_name);

		$filename = '/content-'.$folder_name;
		$themepath = get_theme_file_path();

		if(!file_exists($themepath.'/template-parts')){
			@mkdir($themepath . '/template-parts', 0777);
			if(!file_exists($themepath.'/template-parts/blocks/')){
				@mkdir($themepath . '/template-parts/blocks/', 0777);
			}
		}

		if(file_exists($themepath.'/template-parts')){
			if(!file_exists($themepath.'/template-parts/blocks/')){
				@mkdir($themepath . '/template-parts/blocks/', 0777);
			}
		}

	
		if (!file_exists($output_dir . $folder_name)) { // Check folder exists or not

			@mkdir($output_dir . $folder_name, 0777); // Create folder by using mkdir function
	        
	        $newFileName = $output_dir . $folder_name . $filename.".php";
$newFileContent = '<?php
/**
 * Block Name: '.$post->post_title.'
 * This is the template that displays the block '.$folder_name.'.
 *
 * @author '.$user_info->display_name.'
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace(\'acf/\', \'\', $block[\'name\']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . \'-\' . $block[\'id\'];
if( !empty($block[\'anchor\']) ) {
	$id = $block[\'anchor\'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block[\'className\']) ) {
	$classes .= \' \' . $block[\'className\'];
}
if( !empty($block[\'align\']) ) {
	$classes .= \' align\' . $block[\'align\'];
}
if( !empty($block[\'align_text\']) ) {
	$classes .= \' align-text-\' . $block[\'align_text\'];
}
if( !empty($block[\'align_content\']) ) {
	$classes .= \' is-position-\' . $block[\'align_content\'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php
	/** 
	 * Here you can add your own code for the Block output
	 *
	 * If you would like to use the <InnerBlocks /> Component
	 * Please make sure you have enabled the JSX Support in your Block
	 *
	 * For more information check @Link https://www.advancedcustomfields.com/resources/acf_register_block_type/
	 * and check the Adding inner blocks section on this page
	 */
	?>
</div>';

			if (file_put_contents($newFileName, $newFileContent) !== false) {
				echo "File created (" . basename($newFileName) . ")";
				
			    if($enqueue_style != false ){
			    	$cssfile ="/".$folder_name;
			    	$newFilestyle = $output_dir . $folder_name . $cssfile.".css";
			    	$newFilestyleContent = "";

			    	if (file_put_contents($newFilestyle, $newFilestyleContent) !== false) {
			    		echo "File created (" . basename($newFilestyle) . ")";
			    	} else {
			    		echo "File created (" . basename($newFilestyle) . ")";
			    	};
				} else {
					$_SESSION['stylefile'] = 'show';
				};

			    if($enqueue_script != false ){
			    	$jsfile ="/".$folder_name;
			    	$newFilescript = $output_dir . $folder_name . $jsfile.".js";
					$newFilescriptContent = "";
					$functionName = str_replace("-", "", $folder_name);
					ob_start();
					?>
(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlock<?php echo $functionName; ?> = function() {
	
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=<?php echo $folder_name; ?>', initializeBlock<?php echo $functionName; ?> );
	} else {
		$(document).ready(function(){
			initializeBlock<?php echo $functionName; ?>();
		});
	}
})(jQuery);
					<?php
					$newFilescriptContent = ob_get_clean();
					
			    	if (file_put_contents($newFilescript, $newFilescriptContent) !== false) {
			    		echo "File created (" . basename($newFilescript) . ")";
			    	} else {
			    		echo "File created (" . basename($newFilescript) . ")";
			    	};
			    	
			    } else {
			    	$_SESSION['scriptfile'] = 'show';
			    };

			} else {
			    echo "Cannot create file (" . basename($newFileName) . ")";
			};

		} else {

			if($enqueue_style != false ){
		    	$cssfile ="/".$folder_name;
		    	$newFilestyle = $output_dir . $folder_name . $cssfile.".css";
				$newFilestyleContent = "";
				
		    	if(!file_exists($newFilestyle)){
		    		if (file_put_contents($newFilestyle, $newFilestyleContent) !== false) {
		    			echo "File created (" . basename($newFilestyle) . ")";
			    	} else {
			    		echo "File created (" . basename($newFilestyle) . ")";
			    	};
		    	};
		    	
		    } else {
		    	$_SESSION['stylefile'] = 'show';
		    };

			if($enqueue_script != false ){
		    	$jsfile ="/".$folder_name;
		    	$newFilescript = $output_dir . $folder_name . $jsfile.".js";
		    	$newFilescriptContent = "";
				$functionName = str_replace("-", "", $folder_name);
				ob_start();
				?>
(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlock<?php echo $functionName; ?> = function() {
	
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=<?php echo $folder_name; ?>', initializeBlock<?php echo $functionName; ?> );
	} else {
		$(document).ready(function(){
			initializeBlock<?php echo $functionName; ?>();
		});
	}
})(jQuery);
				<?php
				$newFilescriptContent = ob_get_clean();
		    	if(!file_exists($newFilescript)){
			    	if (file_put_contents($newFilescript, $newFilescriptContent) !== false) {
			    		echo "File created (" . basename($newFilescript) . ")";
			    	} else {
			    		echo "File created (" . basename($newFilescript) . ")";
			    	};
		    	};
			} else {
		    	$_SESSION['scriptfile'] = 'show';
		    };
		};

	}

	// Add Meta Box
	public function block_metaboxes() {
		global $post;
		if(!isset($post))
			return;
		if($post->post_status == 'publish'){
			add_meta_box(
				'block_location',
				'Template',
				[ $this, 'block_location' ],
				'mo-block',
				'side',
				'default'
			);
		};
	}


	// Meta box action function
	function block_location(){
		global $post;
		$output_dir  = get_theme_file_path('/template-parts/blocks/');
		$theme_root = get_theme_root();
		$post_content = json_decode($post->post_content);
		$folder_name = $post_content->name;
		if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $folder_name)) {
			$folder_name = 	preg_replace('/[^A-Za-z0-9\-]/', '-', $folder_name);
		} else {
			$folder_name = $post_content->name;
		};

		$enqueue_style = $post_content->enqueue_style;
		$enqueue_script = $post_content->enqueue_script;
		$meta_html = '';
		$cssfile ="/".$folder_name;
		$jsfile ="/".$folder_name;
		$phpfile = '/content-'.$folder_name;

		$folderpath = $output_dir . $folder_name;
		$folder = '';
		$php_File = '';
		$css_File = '';
		$js_File = '';
		$phppath = $output_dir . $folder_name . $phpfile.'.php';
		$csspath = $output_dir . $folder_name . $cssfile.'.css';
		$jspath  = $output_dir . $folder_name . $jsfile.'.js';

		if (file_exists($output_dir . $folder_name)) {

			if(file_exists($phppath)){
				$php_File = '<div class="phptemplate notice-success">
								<h3> '.__("Template found", "acf-block-generator").' </h3>
								<p> '.__("We have found your PHP file at the following location: ", "acf-block-generator").' </p>
								<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$phppath) . '</span>
							</div>';
			} else {
				$php_File = '<div class="phptemplate  notice-error">
								<h3> '.__("Template not found", "maatwerkonline").' </h3>
								<p> '.__("To display this Block we look for the following file: ", "maatwerkonline").' </p>
								<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$phppath) . '</span>
							</div>';
			};

			if(!empty($enqueue_style)){
				if(file_exists($csspath)){
					$css_File = '<div class="csstemplate  notice-success">
								<h3> '.__("CSS file found", "acf-block-generator").' </h3>
								<p> '.__("We have found your CSS file at the following location: ", "acf-block-generator").' </p>
								<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$csspath) . '</span>
							</div>';
				} else {
					$css_File = '<div class="csstemplate  notice-error">
								<h3> '.__("CSS file not found", "maatwerkonline").' </h3>
								<p> '.__("To display this Block CSS we look for the following file: ", "maatwerkonline").' </p>
								<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$csspath) . '</span>
							</div>';
				} ;
			} else {
				$css_File =  '';
			};

			
			if(!empty($enqueue_script)){
				if(file_exists($jspath)){
					$js_File = '<div class="jstemplate notice-success">
									<h3> '.__("JS file found", "acf-block-generator").' </h3>
									<p> '.__("We have found your JS file at the following location: ", "maatwerkonline", "acf-block-generator").' </p>
									<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$jspath) . '</span>
								</div>';
				} else {
					$js_File = '<div class="jstemplate notice-error">
									<h3> '.__("JS file not found", "maatwerkonline").' </h3>
									<p> '.__("To display this Block JS we look for the following file: ", "maatwerkonline").' </p>
									<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$jspath) . '</span>
								</div>';
				};
			} else {
				$js_File = '';
			};

		} else {
			$folder = '<div class="folder notice-error">
								<h3> '.__("Block Folder not found", "acf-block-generator").' </h3>
								<p> '.__("To display this Block we look for the following folder: ", "acf-block-generator").' </p>
								<span class="wpfileUrl" style="background: lightgrey">/wp-content/themes' . str_replace($theme_root, "",$folderpath) . '</span>
							</div>';
		};

		$meta_html = '<div class="mainblock"> 
						'.$folder.'
						'.$php_File.'
						'.$css_File.'
						'.$js_File.'
					</div>';

		echo $meta_html;

	}


	// Rename folder, files if block name is changes
	public function check_for_block_title_change($post_ID, $post_after, $post_before){

		// If updated post_type is not mo-block, don't continue
		if($post_before->post_type != 'mo-block'){
			return;
		}

		$post 		    = get_post($post_ID);
		$previous_title = $post_before->post_title;
		$current_title 	= $post_after->post_title;
		$post_content 	= $post_after->post_content;
		$output_dir     = get_theme_file_path("/template-parts/blocks/");
		
		$oldlowerarr    = strtolower($post_before->post_title);
		$oldfoldername  = str_replace(" ","-",$oldlowerarr);
		$oldfilename    = '/content-'.$oldfoldername;
		$oldassetsfile  = "/".$oldfoldername;
		
		$newlowerarr    = strtolower($post_after->post_title);
		$newfoldername  = str_replace(" ","-",$newlowerarr);
		$newfilename    = '/content-'.$newfoldername;
		$newassetsfile  = "/".$newfoldername;

		if( ($previous_title != $current_title) && $previous_title != '' && $current_title != '' ){
			if (file_exists($output_dir . $oldfoldername)){
				
				$oldFileName_php = $output_dir . $oldfoldername . $oldfilename.".php";	
				$oldFileName_css = $output_dir . $oldfoldername . $oldassetsfile.".css";
				$oldFileName_js = $output_dir . $oldfoldername . $oldassetsfile.".js";

				$newFileName_php = $output_dir . $oldfoldername . $newfilename.".php";	
				$newFileName_css = $output_dir . $oldfoldername . $newassetsfile.".css";
				$newFileName_js = $output_dir . $oldfoldername . $newassetsfile.".js";

				if (file_exists($oldFileName_js)){
					rename($oldFileName_js , $newFileName_js);
				};

				if (file_exists($oldFileName_php)){
					rename($oldFileName_php , $newFileName_php);
				};
				
				if (file_exists($oldFileName_css)){
					rename($oldFileName_css, $newFileName_css);
				};
				
				rename($output_dir . $oldfoldername, $output_dir . $newfoldername);

				$_SESSION["blocks_name"] = $post_ID;

			};
		};

		return $data;

		wp_reset_postdata();

	}


	// Remove CSS and JS file
	public function remove_block_file(){
	 
		 if(isset($_REQUEST['block_id']) && !empty($_REQUEST['block_id']) ){
			$post = get_post($_REQUEST['block_id']);
			$output_dir  = get_theme_file_path("/template-parts/blocks/");
			
			$post_content = json_decode($post->post_content);

			$folder_name = $post_content->name;
			$cssfile ="/".$folder_name;
			$jsfile ="/".$folder_name;

			if (file_exists($output_dir . $folder_name)){

				if(isset($_REQUEST['css']) && $_REQUEST['css'] == true  ){
					$cssfilepath = $output_dir . $folder_name . $cssfile.'.css';	
					$jsfilepath = $output_dir . $folder_name . $jsfile.'.js';
					unlink($cssfilepath);
					$_SESSION["cssfile"] = true;

					if(file_exists($jsfilepath)){
						$_SESSION['scriptfile'] = 'show';
					};
				};

				if(isset($_REQUEST['js']) && $_REQUEST['js'] == true  ){

					$cssfilepath = $output_dir . $folder_name . $cssfile.'.css';
					$jsfilepath = $output_dir . $folder_name . $jsfile.'.js';
					unlink($jsfilepath);
					$_SESSION["jsfile"] = true;

					if(file_exists($cssfilepath)){
						$_SESSION['stylefile'] = 'show';
					};

				};
				
			};

		};


		// Remove folder, files
		if(isset($_REQUEST['block']) && !empty($_REQUEST['block']) ){

			$output_dir  = get_theme_file_path("/template-parts/blocks/");
			$folder_name = $_REQUEST['block'];
			$folder_path = $output_dir . $folder_name;
			$cssfilepath = $output_dir . $folder_name . '/'.$folder_name.'.css';	
			$jsfilepath = $output_dir . $folder_name . '/'.$folder_name.'.js';
			$phpfilepath = $output_dir . $folder_name . '/'.'content-'.$folder_name.'.php';

			if(file_exists($cssfilepath)){
				unlink($cssfilepath);
			};

			if(file_exists($jsfilepath)){
				unlink($jsfilepath);
			};

			if(file_exists($phpfilepath)){
				unlink($phpfilepath);
			};

			if(!file_exists($cssfilepath) && !file_exists($jsfilepath) && !file_exists($phpfilepath)  ){
				rmdir($folder_path);	
			};
			
		};

	}


	// Managed all admin notice
	public function block_admin_notice(){
		global $post;
	    $screen = get_current_screen();

		// Delete notice	
		if( isset($_SESSION['delete_block']) && !empty($_SESSION['delete_block']) ){
	    	$class = 'notice notice-info is-dismissible';
	        $message = sprintf( __( 'You have permanently removed the block %s. Do you want to delete the folder of this block also to keep the server clean?', 'acf-block-generator' ), $_SESSION['delete_block'] );
	        printf( '<div class="%1$s"><p>%2$s<a id="scriptClass" href="edit.php?post_status=trash&post_type=mo-block&block='.$_SESSION['delete_block'].'"> Yes, please delete the folder with the files</a></p></div>', esc_attr( $class ), esc_html( $message ) ); 
	        unset($_SESSION['delete_block']);
	    };

	    // Block exist notice
	    if($screen->parent_base == 'edit' && isset($_SESSION['duplicate_block']) && !empty($_SESSION['duplicate_block']) ){
	    	 echo '<div class="error notice is-dismissible">
	    	 		<p>'.__("This Block Name is already in use, please choose another Block Name", 'acf-block-generator').'</p></div>';
	    	 unset($_SESSION['duplicate_block']);
	    };

		$editPostScreen = ($screen->action != 'add' && $screen->parent_base == 'edit' && $screen->post_type == 'mo-block') ? true : false;

		if ($editPostScreen && $post != '') {
			$post_content = json_decode($post->post_content);
			$enqueue_style = $post_content->enqueue_style;
			$enqueue_script = $post_content->enqueue_script;

			$output_dir  = get_theme_file_path("/template-parts/blocks/");
			$folder_name = $post_content->name;
			$cssfile ="/".$folder_name;
			$jsfile ="/".$folder_name;
			$cssfilepath = $output_dir . $folder_name . $cssfile.'.css';	
			$jsfilepath = $output_dir . $folder_name . $jsfile.'.js';	
		}
	    
	    // Enqueue style notice
		if ($editPostScreen && $post != '' && $enqueue_style == false && file_exists($cssfilepath) && !empty($_SESSION['stylefile']) && $post->post_status == 'publish' ) {
			$class = 'notice notice-info is-dismissible';
	        $message = __( "You have unchecked the 'Enqueue Style' checkbox. For safety reasons we didn't yet deleted the CSS file for you. Do you want to delete the CSS file also to keep the server clean? ", 'acf-block-generator' );
	        printf( '<div class="%1$s"><p>%2$s<a id="scriptClass" href="post.php?post='.$post->ID.'&action=edit&block_id='.$post->ID.'&css=true">Yes, please delete the CSS file</a></p></div>', esc_attr( $class ), esc_html( $message ) ); 
	        unset($_SESSION['stylefile']);
		};

		// Enqueue script notice
		if ($editPostScreen && $post != '' && $enqueue_script == false && file_exists($jsfilepath) && !empty($_SESSION['scriptfile']) && $post->post_status == 'publish' )  {
			$class = 'notice notice-info is-dismissible';
	        $message = __( "You have unchecked the 'Enqueue Script' checkbox. For safety reasons we didn't yet deleted the JS file for you. Do you want to delete the JS file also to keep the server clean?  ", 'acf-block-generator' );
	        printf( '<div class="%1$s"><p>%2$s<a class="styleClass" href="post.php?post='.$post->ID.'&action=edit&block_id='.$post->ID.'&js=true">Yes, please delete the JS file</a></p></div>', esc_attr( $class ), esc_html( $message ) ); 
	        unset($_SESSION['scriptfile']);
		};

		// Block rename
		if(!empty($_SESSION['blocks_name'])){ ?>
			<div class="notice notice-success is-dismissible">
				<p><?php _e("We updated the Block files in your theme for you, so you don't have to worry about this.", 'acf-block-generator'); ?></p>
			</div>
			<?php unset($_SESSION['blocks_name']); 
		};

		// Delete css file notice
		if(!empty($_SESSION['cssfile']) && $_SESSION['cssfile'] == true ){ ?>
			<div class="notice notice-success is-dismissible">
				<p><?php _e("CSS file is deleted correctly.", 'acf-block-generator'); ?></p>
			</div>
			<?php unset($_SESSION['cssfile']); 
		};

		// Delete js file notice
		if(!empty($_SESSION['jsfile']) && $_SESSION['jsfile'] == true ){ ?>
			<div class="notice notice-success is-dismissible">
				<p><?php _e("JS file is deleted correctly.", 'acf-block-generator'); ?></p>
			</div>
			<?php unset($_SESSION['jsfile']); 
		};

	}


	// Add admin CSS
	public function my_admin_css() {
		echo '<style>
			#poststuff #block_location .inside{margin:0; padding: 0 15px;}
			#poststuff #block_location .inside h3{margin-bottom:-5px; margin-top: 0;     font-size: 14px;}
			#poststuff #block_location .inside .folder, #poststuff  #block_location .inside .phptemplate, #poststuff  #block_location .inside .csstemplate, #poststuff  #block_location .inside .jstemplate{position:relative; display: inline-block; padding:15px 0; word-break: break-word;}
			#poststuff #block_location .inside .notice-success:before, #poststuff  #block_location .inside .notice-error:before{width:4px; height:100%; position:absolute; left:-15px; top:0; background: #46b450; content: ""}
			#poststuff #block_location .inside .notice-error:before{background: #db2525;}
			#poststuff #block_location .inside .wpfileUrl{display: inline-block; padding:5px; background:#ededed!important; border-radius:3px;}
	  	</style>';
	}

	// Delete block
	public function block_delete($post_id){
		if(get_post_type($post_id) == 'mo-block') {
			$post = get_post($post_id);

			if(!empty($post)){
				$block_data = json_decode($post->post_content);
				$_SESSION['delete_block'] = $block_data->name;
			};

			wp_reset_postdata();
		}
	}

	// Remove bulk delete from mo-block
	public function block_bulk_delete( $actions  ){
		unset( $actions['delete'] );
		return $actions;
	}

	// Check duplicate block exist
	public function check_blockexist_callback( $pid ) {
	    global $wpdb;
	    $post = get_post( $pid );
	    if($post->post_type == 'mo-block'){
		   	$table_name = $wpdb->prefix.'posts';	
			$post_title = strtolower($post->post_title);
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `post_title`='$post_title' AND `post_type`= 'mo-block' AND `post_status` = 'publish' ");

		    foreach( (array)$results as $event ) {
				if ( $event->ID == $post->ID ) continue;
				
		        if ( strtolower($event->post_title) == strtolower($post->post_title) ) {
		        	wp_delete_post($post->ID);
		        	$_SESSION['duplicate_block'] = $post->ID;
		        	unset($_SESSION['delete_block']);
		        	$redirectURL = get_site_url().'/wp-admin/post-new.php?post_type=mo-block';
		        	header("Location:".$redirectURL);
					exit();
				};
		    };
			wp_reset_postdata();
		}
	}

}