<?php
// ACF Callback function
function my_acf_block_render_callback( $block ) {

	// convert name ("acf/testimonial") into path friendly slug
	$slug = str_replace('acf/', '', $block['name']);

	// Include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path( "/template-parts/blocks/{$slug}/content-{$slug}.php" ) ) ) {
		include( get_theme_file_path( "/template-parts/blocks/{$slug}/content-{$slug}.php" ) );
	};

}