<?php
/**
 * The Template for displaying all single quizzes when using Course Theme.
 *
 * Override this template by copying it to yourtheme/sensei/course-theme/single-quiz.php
 *
 * @author      Automattic
 * @package     Sensei
 * @category    Templates
 * @version     3.13.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<!-- wp:sensei-lms/quiz-back-to-lesson /-->

<!-- wp:html -->
<div>[TODO: Quiz content here]</div>
<!-- /wp:html -->
