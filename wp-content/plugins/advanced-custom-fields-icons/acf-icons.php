<?php
/*
  Plugin Name: Advanced Custom Fields PRO Icons Add-On
  Description: Adds a new Icon to the popular Advanced Custom Fields plugin.
  Version: 1.0.0
  Author: Maatwerk Online
  Author URI: https://www.maatwerkonline.nl/
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!defined('ACFCI_VERSION')) {
    define('ACFCI_VERSION', '1.0.0');
}

if (!defined('ACFCI_PUBLIC_PATH')) {
    define('ACFCI_PUBLIC_PATH', plugin_dir_url(__FILE__));
}

if (!defined('ACFCI_BASENAME')) {
    define('ACFCI_BASENAME', plugin_basename(__FILE__));
}

if (is_admin()) {
    require 'admin/class-ACFCI-Admin.php';
}

if (!class_exists('acf_plugin_custom_icons')) :

    class acf_plugin_custom_icons {

        public function init() {
            $this->settings = array(
                'version' => ACFCI_VERSION,
                'url' => plugin_dir_url(__FILE__),
                'path' => plugin_dir_path(__FILE__)
            );

            load_plugin_textdomain('acf-custom-icons', false, plugin_basename(dirname(__FILE__)) . '/lang');
            $icon_sets_list = get_option('acfci_settings');
            if (isset($icon_sets_list['acfci_icons_bigmug'])) {
                include_once('fields/acf-big-mug.php');
                include_once('assets/css/fonts/big_mug/big_mug.php');
            }
            if (isset($icon_sets_list['acfci_icons_elegant'])) {
                include_once('fields/acf-elegant.php');
                include_once('assets/css/fonts/elegant/elegant.php');
            }
            if (isset($icon_sets_list['acfci_icons_entypo'])) {
                include_once('fields/acf-entypo.php');
                include_once('assets/css/fonts/entypo/entypo.php');
            }
            if (isset($icon_sets_list['acfci_icons_google_material'])) {
                include_once('fields/acf-google-material.php');
                include_once('assets/css/fonts/google_material/google_material.php');
            }
            if (isset($icon_sets_list['acfci_icons_icomoon'])) {
                include_once('fields/acf-icomoon.php');
                include_once('assets/css/fonts/icomoon/icomoon.php');
            }
            if (isset($icon_sets_list['acfci_icons_ionicon'])) {
                include_once('fields/acf-ionicon.php');
                include_once('assets/css/fonts/ionicon/ionicon.php');
            }
            if (isset($icon_sets_list['acfci_icons_social_icons'])) {
                include_once('fields/acf-social-icons.php');
                include_once('assets/css/fonts/social_icons/social_icons.php');
            }
            if (isset($icon_sets_list['acfci_icons_linear_icons'])) {
                include_once('fields/acf-linearicons.php');
                include_once('assets/css/fonts/linearicons/linearicons.php');
            }
            if (isset($icon_sets_list['acfci_icons_typicons'])) {
                include_once('fields/acf-typicons.php');
                include_once('assets/css/fonts/typicons/typicons.php');
            }
            if (isset($icon_sets_list['acfci_icons_whhg'])) {
                include_once('fields/acf-whhg.php');
                include_once('assets/css/fonts/whhg/whhg.php');
            }

            foreach ($icon_sets_list as $key => $value) {

                
                if (strpos($key, 'custom') !== false) {
                    global $icon_set_retrieve;
                    $key = str_replace("acfci_custom_icons_", "", $key);
                    $icon_set_retrieve = $key;
                    include_once(get_stylesheet_directory() . '/assets/icons/'.$key.'/'.$key.'.php');
                    include_once('fields/acf-custom.php');
                }
            }
        }

    }

    add_action('acf/include_field_types', array(new acf_plugin_custom_icons, 'init'), 10);

endif;

