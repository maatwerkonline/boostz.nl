<?php

/**
 * =======================================
 * Advanced Custom Fields Font Awesome Admin
 * =======================================
 * 
 * 
 * @author Matt Keys <https://profiles.wordpress.org/mattkeys>
 */
class ACFCI_Admin {

    private $version;

    public function init() {
        add_action('admin_notices', array($this, 'show_upgrade_notice'));
        add_action('admin_notices', array($this, 'maybe_notify_cdn_error'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_filter('plugin_action_links', array($this, 'add_settings_link'), 10, 2);
        add_action('admin_menu', array($this, 'add_settings_page'), 100);
        add_action('admin_init', array($this, 'register_settings'));
    }

    public function show_upgrade_notice() {
        $acfci_settings = get_option('acfci_settings');
        if (!isset($acfci_settings['show_upgrade_notice'])) {
            return;
        }
        ?>
        <div class="notice notice-info is-dismissible">
            <p><?php echo sprintf(__('Visit the new ACF <a href="%s">Icons Settings</a> page to change Custom Icons settings.', 'acf-custom-icons'), admin_url('/edit.php?post_type=acf-field-group&page=customicons-settings')); ?></p>
        </div>
        <?php
        unset($acfci_settings['show_upgrade_notice']);
        update_option('acfci_settings', $acfci_settings, false);
    }

    public function maybe_notify_cdn_error() {
        if (!get_option('ACFCI_cdn_error')) {
            return;
        }

        delete_option('ACFCI_cdn_error');
        $curl_info = curl_version();
        ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('The plugin "Advanced Custom Fields: Custom Icons" has detected an error while retrieving the latest FontAwesome icons. This may be due to temporary CDN downtime. However if problems persist, please contact your hosting provider to ensure cURL is installed and up to date. Detected cURL version: ', 'acf-custom-icons') . $curl_info['version']; ?></p>
        </div>
        <?php
    }

    public function enqueue_scripts($hook) {
//        print_r($hook);
        if ('custom-fields_page_customicons-settings' == $hook) {
            return;
        }
//        wp_enqueue_style('acfci-bootstrap-min', ACFCI_PUBLIC_PATH . 'assets/css/bootstrap.min.css');
        wp_enqueue_style('acfci-bootstrap-select', ACFCI_PUBLIC_PATH . 'assets/css/bootstrap-select.min.css');
        wp_enqueue_style('acfci-input', ACFCI_PUBLIC_PATH . 'assets/css/input.css');
        wp_enqueue_script('acfci-bootstrap-min', ACFCI_PUBLIC_PATH . 'assets/js/bootstrap.min.js', array(), '1.0.0', true);
        wp_enqueue_script('acfci-bootstrap-select', ACFCI_PUBLIC_PATH . 'assets/js/bootstrap-select.min.js', array(), '1.0.0', true);
        wp_enqueue_script('acfci-input', ACFCI_PUBLIC_PATH . 'assets/js/input.js', array('jquery'), '1.0.0', true);
        wp_localize_script('acfci-input', 'ACFCI', array(
            'search_string' => __('Search List', 'acf-custom-icons')
        ));
    }

    public function add_settings_link($links, $file) {
        if ($file != ACFCI_BASENAME) {
            return $links;
        }

        array_unshift($links, '<a href="' . esc_url(admin_url('/edit.php?post_type=acf-field-group&page=customicons-settings')) . '">' . esc_html__('Settings', 'acf-custom-icons') . '</a>');

        return $links;
    }

    public function add_settings_page() {
        $capability = apply_filters('acf/settings/capability', 'manage_options');

        add_submenu_page(
                'edit.php?post_type=acf-field-group',
                'Icons Settings',
                'Icons Settings',
                $capability,
                'customicons-settings',
                array($this, 'customicons_settings')
        );
    }

    public function customicons_settings() {
        $errors = get_settings_errors('acfci_messages');
        if (isset($_GET['settings-updated']) && !$errors) {
            add_settings_error('acfci_messages', 'acfci_message', __('Settings Saved', 'acf-custom-icons'), 'updated');
        }

        settings_errors('acfci_messages');
        ?>
        <div class="wrap">
            <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
            <form action="options.php" method="post">
                <?php
                settings_fields('acfci');

                do_settings_sections('acfci');

                submit_button('Save Settings');
                ?>
            </form>
        </div>
       

        <?php
    }

    public function register_settings() {
        register_setting(
                'acfci',
                'acfci_settings',
                array()
        );

        add_settings_section(
                'acfci_section_developers',
                __('', 'acf-custom-icons'),
                array($this, ''),
                'acfci'
        );

        add_settings_field(
                'acfci_icons_bigmug',
                __('BigMug', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_bigmug',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'big_mug',
                    'checkbox_title' => __('Enable BigMug Icons', 'acf-custom-icons'),
                    'a_title' => __('Big Mug (font: big_mug)', 'acf-custom-icons'),
                    'description' => __('Check this to enable BigMug icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_entypo',
                __('Entypo', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_entypo',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'entypo',
                    'checkbox_title' => __('Enable Entypo Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: entypo)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Entypo icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_elegant',
                __('Elegant', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_elegant',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'elegant',
                    'checkbox_title' => __('Enable Elegant Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: elegant)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Elegant icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_social_icons',
                __('Social Icons', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_social_icons',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'social_icons',
                    'checkbox_title' => __('Enable Social Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: social_icons)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Social Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_linearicons',
                __('Linear Icons', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_linear_icons',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'linearicons',
                    'checkbox_title' => __('Enable Linear Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: linearicons)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Linear Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_google_material',
                __('Google Material', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_google_material',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'google_material',
                    'checkbox_title' => __('Enable Google Material Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: google_material)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Google Material Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_icomoon',
                __('Icomoon', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_icomoon',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'icomoon',
                    'checkbox_title' => __('Enable Icomoon Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: icomoon)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Icomoon Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_ionicon',
                __('Icomoon', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_ionicon',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'ionicon',
                    'checkbox_title' => __('Enable Ionicon Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: ionicon)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Ionicon Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_typicons',
                __('Typicons', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_typicons',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'typicons',
                    'checkbox_title' => __('Enable Typicons Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: typicons)', 'acf-custom-icons'),
                    'description' => __('Check this to enable Typicons Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );
        add_settings_field(
                'acfci_icons_whhg',
                __('WebHostingHubGlyphs', 'acf-custom-icons'),
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_icons_whhg',
                    'class' => 'acfci_row pro_icons',
                    'floder' => 'typicons',
                    'checkbox_title' => __('Enable WebHostingHubGlyphs Icons', 'acf-custom-icons'),
                    'a_title' => __('Entypo (font: whhg)', 'acf-custom-icons'),
                    'description' => __('Check this to enable WebHostingHubGlyphs Icons. Complete list of icons and their names can be found', 'acf-custom-icons')
                )
        );

        // Get fonts and generate an option for ite

        foreach (glob(get_stylesheet_directory() . '/assets/icons/*/*.css') as $file) {
            $filename = substr($file, strrpos($file, '/') + 1);
            $base_name = str_replace(".css", "", $filename);
            $base_name = str_replace("-", "_", $base_name);
            add_settings_field(
                'acfci_custom_icons_'.$base_name,
                $base_name,
                array($this, 'acfci_icon_checkbox'),
                'acfci',
                'acfci_section_developers',
                array(
                    'label_for' => 'acfci_custom_icons_'.$base_name,
                    'class' => 'acfci_row pro_icons',
                    'floder' => $base_name,
                    'checkbox_title' => 'Enable '.$base_name,
                    'a_title' => __('Entypo (font: '.$base_name.')', 'acf-custom-icons'),
                )
            );
        }
    }

    public function acfci_icon_checkbox($args) {
        $options = get_option('acfci_settings');
        ?>
        <p>
            <input type="checkbox" value="1" id="<?php echo esc_attr($args['label_for']); ?>" name="acfci_settings[<?php echo esc_attr($args['label_for']); ?>]" <?php echo isset($options[$args['label_for']]) ? ( checked($options[$args['label_for']]) ) : ( '' ); ?> />
            <label for="<?php echo esc_attr($args['label_for']); ?>"><?php echo esc_attr($args['checkbox_title']); ?></label>
        </p>
        <?php add_thickbox(); ?>
        <?php if(!empty($args['description'])) : ?>
            <p><?php echo esc_attr($args['description']); ?> <a href="<?php echo ACFCI_PUBLIC_PATH . '/assets/css/fonts/' . esc_attr($args['floder']) . '/demo.html?TB_iframe=true&width=650&height=550' ?>" title="<?php echo esc_attr($args['a_title']); ?>"  class="thickbox" ><?php _e('here', 'maatwerkonline-page-builder') ?></a>.</p>
        <?php endif; ?>
        <?php
    }

}

add_action('acf/init', array(new ACFCI_Admin, 'init'), 10);
