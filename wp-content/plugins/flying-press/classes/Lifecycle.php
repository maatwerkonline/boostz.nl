<?php
namespace FlyingPress;

class Lifecycle
{
  public static function init()
  {
    // Plugin activation
    register_activation_hook(FLYING_PRESS_FILE, ['FlyingPress\Lifecycle', 'on_activation']);

    // Plugin deactivation
    register_deactivation_hook(FLYING_PRESS_FILE, ['FlyingPress\Lifecycle', 'on_deactivation']);

    // Plugin deletion
    register_uninstall_hook(FLYING_PRESS_FILE, ['FlyingPress\Lifecycle', 'on_deletion']);
  }

  public static function on_activation()
  {
    // Set define('WP_CACHE', true);
    self::add_wp_cache_constant();

    // Set advanced-cache.php
    self::add_advanced_cache();

    // Add htaccess rules
    self::add_htaccess_rules();

    // Keyless activation if found
    License::keyless_activate();
  }

  public static function on_deactivation()
  {
    // Set define('WP_CACHE', false);
    self::remove_wp_cache_constant();

    // Remove advanced-cache.php
    self::remove_advanced_cache();

    // Remove htaccess rules
    self::remove_htaccess_rules();
  }

  public static function on_deletion()
  {
    Purge::purge_entire_cache();
  }

  public static function add_wp_cache_constant()
  {
    // Get file location of wp-config
    $wp_config_file = FLYING_PRESS_ABSPATH . 'wp-config.php';

    // If file not found, get file from parent directory
    if (!is_file($wp_config_file)) {
      $wp_config_file = dirname(FLYING_PRESS_ABSPATH) . '/wp-config.php';
    }

    // Make sure file exists and is writable
    if (!is_file($wp_config_file) || !is_writable($wp_config_file)) {
      return;
    }

    // String we need to insert
    $content = "\ndefine('WP_CACHE', true); // Added by FlyingPress";

    // Grab contents
    $wp_config = file_get_contents($wp_config_file);

    // Remove any existing WP_CACHE config
    $regex_for_wp_cache = '/define\([\'\"]WP_CACHE[\'\"].*/';
    $wp_config = preg_replace($regex_for_wp_cache, '', $wp_config);

    // Add our own WP_CACHE
    $wp_config = str_replace('<?php', '<?php' . $content, $wp_config);

    // Write back to file
    file_put_contents($wp_config_file, $wp_config);
  }

  public static function remove_wp_cache_constant()
  {
    // Get file location of wp-config
    $wp_config_file = FLYING_PRESS_ABSPATH . 'wp-config.php';

    // If file not found, get file from parent directory
    if (!is_file($wp_config_file)) {
      $wp_config_file = dirname(FLYING_PRESS_ABSPATH) . '/wp-config.php';
    }

    // Make sure file exists and is writable
    if (!is_file($wp_config_file) || !is_writable($wp_config_file)) {
      return;
    }

    // String we need to insert
    $content = "\ndefine('WP_CACHE', true); // Added by FlyingPress";

    // Grab contents
    $wp_config = file_get_contents($wp_config_file);

    $wp_config = str_replace($content, '', $wp_config);

    // Write back to file
    file_put_contents($wp_config_file, $wp_config);
  }

  public static function add_advanced_cache()
  {
    $advanced_cache_contents = file_get_contents(
      FLYING_PRESS_PLUGIN_DIR . 'assets/advanced-cache.php'
    );

    $advanced_cache_contents = str_replace(
      'QUERY_STRING_REGEX_TO_REPLACE',
      Cache::get_ignore_queries_regex(),
      $advanced_cache_contents
    );

    file_put_contents(WP_CONTENT_DIR . '/advanced-cache.php', $advanced_cache_contents);
  }

  public static function remove_advanced_cache()
  {
    @unlink(WP_CONTENT_DIR . '/advanced-cache.php');
  }

  public static function add_htaccess_rules()
  {
    $htaccess_file = FLYING_PRESS_ABSPATH . '.htaccess';

    if (!is_readable($htaccess_file) || !is_writeable($htaccess_file)) {
      return;
    }

    // Get existing rules
    $htaccess_contents = file_get_contents($htaccess_file);

    // Remove existing htaccess rules
    $htaccess_contents = ltrim(
      preg_replace(
        '/\s*# BEGIN FlyingPress.*# END FlyingPress\s*?/isU',
        PHP_EOL . PHP_EOL,
        $htaccess_contents
      )
    );

    // Rules by FlyingPress
    $flying_press_htaccess = file_get_contents(FLYING_PRESS_PLUGIN_DIR . 'assets/htaccess.txt');

    // Inject web server name (Apache/LiteSpeed)
    $server_name = preg_match('/LiteSpeed/', $_SERVER['SERVER_SOFTWARE']) ? 'LiteSpeed' : 'Apache';
    $flying_press_htaccess = str_replace('[WEB_SERVER_NAME]', $server_name, $flying_press_htaccess);

    // Prepend FlyingPress rules
    $htaccess_contents = str_replace(
      '# BEGIN WordPress',
      $flying_press_htaccess . "\n\n# BEGIN WordPress",
      $htaccess_contents
    );

    // Write back to htaccess
    file_put_contents($htaccess_file, $htaccess_contents);
  }

  public static function remove_htaccess_rules()
  {
    $htaccess_file = FLYING_PRESS_ABSPATH . '.htaccess';

    if (!is_readable($htaccess_file) || !is_writeable($htaccess_file)) {
      return;
    }

    // Get existing rules
    $htaccess_contents = file_get_contents($htaccess_file);

    // Remove existing htaccess rules
    $htaccess_contents = ltrim(
      preg_replace(
        '/\s*# BEGIN FlyingPress.*# END FlyingPress\s*?/isU',
        PHP_EOL . PHP_EOL,
        $htaccess_contents
      )
    );

    // Write back to htaccess
    file_put_contents($htaccess_file, $htaccess_contents);
  }
}