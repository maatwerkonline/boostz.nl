<?php
namespace FlyingPress;

class CDN
{
  public static function add_preconnect($html)
  {
    if (!Config::$config['cdn_enable'] || !Config::$config['cdn_url']) {
      return false;
    }

    $cdn_url = Config::$config['cdn_url'];

    $preconnect_tag = $html->createElement('link', null, [
      'rel' => 'preconnect',
      'href' => $cdn_url,
      'crossorigin' => null,
    ]);

    $html->first('head')->insertAfter($preconnect_tag, $html->first('title'));
  }

  public static function rewrite(&$content)
  {
    // No need to add CDN
    if (!Config::$config['cdn_enable'] || !Config::$config['cdn_url']) {
      return false;
    }

    // Remove http(s): from site url and CDN url
    $site_domain = preg_replace('(^https?:)', '', FLYING_PRESS_SITE_URL);
    $cdn_domain = preg_replace('(^https?:)', '', Config::$config['cdn_url']);

    // Escape for regex
    $site_domain_escaped = preg_quote($site_domain, '/');

    // Files types as regex
    $file_types = [
      'all' => 'css|js|eot|otf|ttf|woff|woff2|gif|jpeg|jpg|png|svg|webp|ico',
      'css_js_font' => 'css|js|eot|otf|ttf|woff|woff2',
      'image' => 'gif|jpeg|jpg|png|svg|webp|ico',
    ];

    // Pick the regex based on `cdn_file_types` config
    $file_types_regex = $file_types[Config::$config['cdn_file_types']];

    // Generate final regex
    // $regex = "/{$home_url_escaped}(\S+?\.({$file_types_regex}))/";
    $regex = "/{$site_domain_escaped}([^\"']*?\.({$file_types_regex})[?\"\'\s)>,])/";

    // Find and replace URLs
    $content = preg_replace($regex, $cdn_domain . '$1', $content);
  }
}
