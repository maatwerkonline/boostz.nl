<?php
namespace FlyingPress;

class Config
{
  public static $config;

  public static function init()
  {
    self::$config = get_option('FLYING_PRESS_CONFIG', []);
    self::migrate_config();
  }

  public static function update_config($new_config)
  {
    $new_config = $new_config + self::$config;
    update_option('FLYING_PRESS_CONFIG', $new_config);
    self::$config = $new_config;
    return $new_config;
  }

  public static function migrate_config()
  {
    // Update or insert (upsert) config when version has changed
    if (get_option('FLYING_PRESS_VERSION') !== FLYING_PRESS_VERSION) {
      // Get current config stored in DB
      $current_config = get_option('FLYING_PRESS_CONFIG', []);

      // Default config. Might change in future
      $default_config = [
        'site_url' => FLYING_PRESS_SITE_URL,
        'license_key' => '',
        'license_status' => '',
        'license_name' => '',
        'license_email' => '',
        'license_expiry' => '',
        'license_beta' => false,
        'cache' => true,
        'cache_auto_purge' => 'none',
        'cache_lifespan' => 'never',
        'cache_excludes' => [],
        'cache_ignore_queries' => [],
        'optimize_logged_in' => false,
        'js_minify' => true,
        'js_preload_links' => true,
        'js_defer' => false,
        'js_defer_inline' => false,
        'js_defer_excludes' => [],
        'js_interaction' => false,
        'js_interaction_includes' => [
          'googletagmanager.com',
          'google-analytics.com',
          'googleoptimize.com',
          'adsbygoogle.js',
          'xfbml.customerchat.js',
          'fbevents.js',
          'widget.manychat.com',
          'cookie-law-info',
          'grecaptcha.execute',
          'static.hotjar.com',
          'hs-scripts.com',
          'embed.tawk.to',
          'disqus.com/embed.js',
          'client.crisp.chat',
          'matomo.js',
          'usefathom.com',
          'code.tidio.co',
          'metomic.io',
          'js.driftt.com',
          'cdn.onesignal.com',
        ],
        'fonts_optimize_google_fonts' => true,
        'fonts_display_swap' => true,
        'fonts_preload_urls' => [],
        'css_minify' => true,
        'css_extract_used' => false,
        'css_unused_method' => 'async',
        'css_force_include_selectors' => [],
        'css_lazy_render_selectors' => ['#comments', 'footer'],
        'img_lazyload' => true,
        'img_lazyload_method' => 'native',
        'img_lazyload_exclude_count' => 2,
        'img_lazyload_excludes' => [],
        'img_disable_emoji' => true,
        'img_width_height' => true,
        'img_preload' => true,
        'img_responsive' => false,
        'iframe_lazyload' => true,
        'iframe_youtube_placeholder' => false,
        'iframe_youtube_placeholder_self_host' => false,
        'cdn_enable' => false,
        'cdn_url' => null,
        'cdn_file_types' => 'all',
        'db_post_revisions' => false,
        'db_post_auto_drafts' => false,
        'db_post_trashed' => false,
        'db_comments_spam' => false,
        'db_comments_trashed' => false,
        'db_transients_expired' => false,
        'db_transients_all' => false,
        'db_optimize_tables' => false,
        'db_schedule_clean' => 'never',
      ];

      // Generate new config by combining current and default config
      $new_config = $current_config + $default_config;

      // Update new config in DB
      update_option('FLYING_PRESS_CONFIG', $new_config);
      self::$config = $new_config;

      // Update version in DB
      update_option('FLYING_PRESS_VERSION', FLYING_PRESS_VERSION);

      // Set advanced-cache.php
      Lifecycle::add_advanced_cache();

      // Set htaccess rules
      Lifecycle::add_htaccess_rules();

      // Keyless activation if found
      License::keyless_activate();
    }
  }
}