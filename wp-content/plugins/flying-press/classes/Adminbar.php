<?php
namespace FlyingPress;

class Adminbar
{
  public static $loading_icon = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDQiIGhlaWdodD0iNDQiIHZpZXdCb3g9IjAgMCA0NCA0NCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBzdHJva2U9IiNmZmYiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc3Ryb2tlLXdpZHRoPSIyIj48Y2lyY2xlIGN4PSIyMiIgY3k9IjIyIiByPSIxIj48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJyIiBiZWdpbj0iMHMiIGR1cj0iMS44cyIgdmFsdWVzPSIxOyAyMCIgY2FsY01vZGU9InNwbGluZSIga2V5VGltZXM9IjA7IDEiIGtleVNwbGluZXM9IjAuMTY1LCAwLjg0LCAwLjQ0LCAxIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9InN0cm9rZS1vcGFjaXR5IiBiZWdpbj0iMHMiIGR1cj0iMS44cyIgdmFsdWVzPSIxOyAwIiBjYWxjTW9kZT0ic3BsaW5lIiBrZXlUaW1lcz0iMDsgMSIga2V5U3BsaW5lcz0iMC4zLCAwLjYxLCAwLjM1NSwgMSIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L2NpcmNsZT48Y2lyY2xlIGN4PSIyMiIgY3k9IjIyIiByPSIxIj48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJyIiBiZWdpbj0iLTAuOXMiIGR1cj0iMS44cyIgdmFsdWVzPSIxOyAyMCIgY2FsY01vZGU9InNwbGluZSIga2V5VGltZXM9IjA7IDEiIGtleVNwbGluZXM9IjAuMTY1LCAwLjg0LCAwLjQ0LCAxIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9InN0cm9rZS1vcGFjaXR5IiBiZWdpbj0iLTAuOXMiIGR1cj0iMS44cyIgdmFsdWVzPSIxOyAwIiBjYWxjTW9kZT0ic3BsaW5lIiBrZXlUaW1lcz0iMDsgMSIga2V5U3BsaW5lcz0iMC4zLCAwLjYxLCAwLjM1NSwgMSIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L2NpcmNsZT48L2c+PC9zdmc+';

  public static function init()
  {
    // Add actions to admin menu bar (quick purge, settings etc)
    add_action('admin_bar_menu', ['FlyingPress\Adminbar', 'add_admin_bar_menu'], 100);

    // Add JS for handling actions from admin bar
    add_action('wp_loaded', ['FlyingPress\Adminbar', 'add_admin_js']);
  }

  public static function add_admin_js()
  {
    if (!is_admin_bar_showing()) {
      return;
    }

    // Scripts required for admin bar
    wp_enqueue_script(
      'flying_press_admin_js',
      FLYING_PRESS_PLUGIN_URL . 'assets/admin.js',
      [],
      FLYING_PRESS_VERSION,
      true
    );

    // Add ajaxurl (espeicially to front-end)
    add_action('wp_head', function () {
      $ajaxurl = admin_url('admin-ajax.php');
      echo "<script>window.ajaxurl = '$ajaxurl'</script>";
    });
  }

  public static function add_admin_bar_menu($admin_bar)
  {
    if (!current_user_can('administrator')) {
      return;
    }

    $title =
      '<span class="ab-icon"><img id="fp-icon" src="' .
      Settings::$menu_icon .
      '" style="width:18px"/>
      <img id="fp-loading-icon" src="' .
      self::$loading_icon .
      '" style="width:18px;display:none"/>
      </span><span class="ab-label">FlyingPress</span>';

    $admin_bar->add_menu([
      'id' => 'flying-press',
      'title' => $title,
      'href' => admin_url('admin.php?page=flying-press'),
    ]);

    $admin_bar->add_menu([
      'id' => 'fp-dashboard',
      'parent' => 'flying-press',
      'title' => 'Dashboard',
      'href' => admin_url('admin.php?page=flying-press'),
    ]);

    if (Config::$config['cache']) {
      $admin_bar->add_menu([
        'id' => 'purge_cached_pages',
        'parent' => 'flying-press',
        'title' => 'Purge Cache',
        'href' => '#',
        'meta' => [
          'onclick' => 'purge_cached_pages()',
        ],
      ]);

      if (!is_admin()) {
        $admin_bar->add_menu([
          'id' => 'purge_current_page',
          'parent' => 'flying-press',
          'title' => 'Purge Current Page',
          'href' => '#',
          'meta' => [
            'onclick' => 'purge_current_page()',
          ],
        ]);
      }

      $admin_bar->add_menu([
        'id' => 'preload_cache',
        'parent' => 'flying-press',
        'title' => 'Purge and Preload Cache',
        'href' => '#',
        'meta' => [
          'onclick' => 'preload_cache()',
        ],
      ]);
    }

    $admin_bar->add_menu([
      'id' => 'purge_css_js_fonts',
      'parent' => 'flying-press',
      'title' => 'Purge CSS/JS/Fonts',
      'href' => '#',
      'meta' => [
        'onclick' => 'purge_css_js_fonts()',
      ],
    ]);

    $admin_bar->add_menu([
      'id' => 'purge_entire_cache',
      'parent' => 'flying-press',
      'title' => 'Purge Critical/Used CSS',
      'href' => '#',
      'meta' => [
        'onclick' => 'purge_entire_cache()',
      ],
    ]);
  }
}