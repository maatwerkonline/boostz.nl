<?php
namespace FlyingPress;

class Shortcuts
{
  public static function init()
  {
    // Register shortcuts
    add_filter('plugin_action_links_' . FLYING_PRESS_FILE, [
      'FlyingPress\Shortcuts',
      'add_shortcuts',
    ]);
  }

  public static function add_shortcuts($links)
  {
    $shortcuts = ['<a href="' . admin_url('admin.php?page=flying-press') . '">Settings</a>'];
    return array_merge($links, $shortcuts);
  }
}
