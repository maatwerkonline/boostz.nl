<?php
namespace FlyingPress;

class Font
{
  public static function add_display_swap_to_google_fonts($html)
  {
    if (!Config::$config['fonts_display_swap']) {
      return true;
    }

    $google_fonts = $html->find('link[href*=fonts.googleapis.com/css]');
    foreach ($google_fonts as $google_font) {
      $google_font->href = $google_font->href . '&display=swap';
    }
  }

  public static function add_display_swap_to_internal_styles($html)
  {
    if (!Config::$config['fonts_display_swap']) {
      return true;
    }

    $stlyes = $html->find('style');

    foreach ($stlyes as $style) {
      $css = $style->text();
      $css = self::inject_display_swap($css);
      if ($css === $style->text()) {
        continue;
      }
      $new_style = $html->createElement('style', $css, [
        'id' => $style->id,
        'media' => $style->media,
      ]);
      $style->replace($new_style);
    }
  }

  public static function optimize_google_fonts($html)
  {
    if (!Config::$config['fonts_optimize_google_fonts']) {
      return true;
    }

    $preconnects = $html->find(
      'link[rel*=pre][href*=fonts.gstatic.com], link[rel*=pre][href*=fonts.googleapis.com]'
    );
    foreach ($preconnects as $preconnect) {
      $preconnect->remove();
    }

    $google_fonts = $html->find('link[href*=fonts.googleapis.com/css]');

    foreach ($google_fonts as $google_font) {
      $hash = substr(md5($google_font->href), 0, 12);
      $file_name = "$hash.google-font.css";

      $file_path = FLYING_PRESS_CACHE_DIR . $file_name;
      $file_url = FLYING_PRESS_CACHE_URL . $file_name;

      if (!is_file($file_path)) {
        self::self_host_style_sheet($google_font->href, $file_path);
      }
      if (is_file($file_path)) {
        $google_font->href = $file_url;
      }
    }
  }

  public static function preload_fonts($html)
  {
    $font_urls = Config::$config['fonts_preload_urls'];

    foreach ($font_urls as $url) {
      $extension = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);
      $preload_tag = $html->createElement('link', null, [
        'rel' => 'preload',
        'href' => $url,
        'as' => 'font',
        'type' => "font/$extension",
        'crossorigin' => null,
      ]);
      $html->first('head')->insertAfter($preload_tag, $html->first('title'));
    }
  }

  public static function inject_display_swap($content)
  {
    if (!Config::$config['fonts_display_swap']) {
      return $content;
    }

    $content = preg_replace(
      '/font-display:\s?(auto|block|fallback|optional)/',
      'font-display:swap',
      $content
    );

    return preg_replace('/@font-face\s*{/', '@font-face{font-display:swap;', $content);
  }

  private static function self_host_style_sheet($url, $file_path)
  {
    // If URL starts with "//", add https
    if (substr($url, 0, 2) === '//') {
      $url = 'https:' . $url;
    }

    // Download style sheet
    $css_file_response = wp_remote_get($url, [
      'user-agent' =>
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36',
    ]);

    // Check Google Fonts returned response
    if (
      is_wp_error($css_file_response) ||
      wp_remote_retrieve_response_code($css_file_response) !== 200
    ) {
      error_log("FlyingPresss: Unable to download Google Font: $url");
      return false;
    }

    // Extract body (CSS)
    $css = $css_file_response['body'];

    // Get list of fonts (woff2 files) inside the CSS
    $font_urls = self::get_font_urls($css);

    self::download_urls_in_parallel($font_urls, FLYING_PRESS_CACHE_DIR);

    foreach ($font_urls as $font_url) {
      $cached_font_url = FLYING_PRESS_CACHE_URL . basename($font_url);
      $css = str_replace($font_url, $cached_font_url, $css);
    }

    file_put_contents($file_path, $css);
  }

  // Source: https://gist.github.com/nicklasos/365a251d63d94876179c
  public static function download_urls_in_parallel($urls, $save_path)
  {
    $multi_handle = curl_multi_init();
    $file_pointers = [];
    $curl_handles = [];

    // Add curl multi handles, one per file we don't already have
    foreach ($urls as $key => $url) {
      $file = $save_path . '/' . basename($url);
      $curl_handles[$key] = curl_init($url);
      $file_pointers[$key] = fopen($file, 'w');
      curl_setopt($curl_handles[$key], CURLOPT_FILE, $file_pointers[$key]);
      curl_setopt($curl_handles[$key], CURLOPT_HEADER, 0);
      curl_setopt($curl_handles[$key], CURLOPT_CONNECTTIMEOUT, 60);
      curl_multi_add_handle($multi_handle, $curl_handles[$key]);
    }

    // Download the files
    do {
      curl_multi_exec($multi_handle, $running);
    } while ($running > 0);

    // Free up objects
    foreach ($urls as $key => $url) {
      curl_multi_remove_handle($multi_handle, $curl_handles[$key]);
      curl_close($curl_handles[$key]);
      fclose($file_pointers[$key]);
    }
    curl_multi_close($multi_handle);
  }

  private static function get_font_urls($css)
  {
    // Extract font urls like: https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2
    $regex = '/url\((https:\/\/fonts\.gstatic\.com\/.*?)\)/';
    preg_match_all($regex, $css, $matches);
    return array_unique($matches[1]);
  }
}
