<?php
namespace FlyingPress;

class Purge
{
  public static function init()
  {
    // Purge necessary URLs when a post is published/updated/deleted
    add_action('save_post', ['FlyingPress\Purge', 'auto_purge_on_save_post']);

    // Purge post URL when comments are udpated
    add_action('wp_update_comment_count', ['FlyingPress\Purge', 'auto_purge_on_comment']);

    // Purge product page when stock is updated in WooCommerce
    add_action('woocommerce_product_set_stock', ['FlyingPress\Purge', 'auto_purge_on_stock']);
    add_action('woocommerce_variation_set_stock', ['FlyingPress\Purge', 'auto_purge_on_stock']);
  }

  public static function auto_purge_on_save_post($post_id)
  {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return;
    }

    if (wp_is_post_autosave($post_id) || wp_is_post_revision($post_id)) {
      return;
    }

    if (get_post_status($post_id) !== 'publish') {
      return;
    }

    $auto_purge = Config::$config['cache_auto_purge'];

    // Purging & preloading entire cache
    if ($auto_purge === 'all') {
      Purge::purge_cached_pages();
      Preload::preload_cache();
      return;
    }

    $urls = [];
    // Add pages URLs to purge & preload
    if ($auto_purge === 'pages') {
      $pages = get_pages();
      $urls = array_map(function ($page) {
        return get_permalink($page->ID);
      }, $pages);
    }

    // Add home page URL
    array_push($urls, FLYING_PRESS_HOME_URL);

    // Blog archive URL
    $page_for_post = get_option('page_for_posts');
    if ($page_for_post) {
      array_push($urls, get_permalink($page_for_post));
    }

    // Add current post URL
    array_push($urls, get_permalink($post_id));

    // Add author profile URL
    array_push($urls, get_author_posts_url($post_id));

    // Find categories of the post
    $categories = get_the_category($post_id);
    foreach ($categories as $category) {
      array_push($urls, get_category_link($category->term_id));
    }

    // WooCommerce
    if (class_exists('woocommerce') && get_post_type($post_id) === 'product') {
      // Shop page URL
      $shop_page_url = get_permalink(wc_get_page_id('shop'));
      array_push($urls, $shop_page_url);

      // Product categories
      $product_categories = get_the_terms($product_id, 'product_cat');
      if ($product_categories) {
        foreach ($product_categories as $product_category) {
          array_push($urls, get_category_link($product_category->term_id));
        }
      }
    }

    $urls = array_unique($urls);
    Purge::purge_by_urls($urls);
    Preload::preload_urls($urls);
  }

  public static function auto_purge_on_comment($post_id)
  {
    $url = get_permalink($post_id);
    Purge::purge_by_urls([$url]);
    Preload::preload_urls([$url]);
  }

  public static function auto_purge_on_stock($product)
  {
    $url = get_permalink($product->get_id());
    Purge::purge_by_urls([$url]);
    Preload::preload_urls([$url]);
  }

  public static function purge_by_urls($urls)
  {
    foreach ($urls as $url) {
      Purge::delete_page_from_url($url);
      do_action('fp_purged_by_url', $url);
    }
  }

  public static function purge_cached_pages()
  {
    $files = Cache::rglob(FLYING_PRESS_CACHE_DIR . '*.html', GLOB_BRACE);
    foreach ($files as $file) {
      @unlink($file);
    }

    do_action('fp_purged_cache');
  }

  public static function purge_css_js_fonts()
  {
    $files = Cache::rglob(FLYING_PRESS_CACHE_DIR . '*.*', GLOB_BRACE);
    foreach ($files as $file) {
      if (!preg_match('/.*(used|critical)\.css/', $file)) {
        @unlink($file);
      }
    }

    do_action('fp_purged_cache');
  }

  public static function purge_entire_cache()
  {
    self::delete_folder(FLYING_PRESS_CACHE_DIR);
    do_action('fp_purged_cache');
  }

  private static function delete_page_from_url($url)
  {
    // E.g.: https://flying-press.com/blog/ to /blog/
    $path = parse_url($url, PHP_URL_PATH);
    $cache_file_path = FLYING_PRESS_CACHE_DIR . $path . 'index.html';
    @unlink($cache_file_path);
  }

  private static function delete_folder($path)
  {
    return is_file($path)
      ? @unlink($path)
      : array_map(__METHOD__, glob($path . '/*')) == @rmdir($path);
  }
}
