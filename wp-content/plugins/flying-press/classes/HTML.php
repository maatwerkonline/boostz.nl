<?php
namespace FlyingPress;

class HTML
{
  public static function init()
  {
    ob_start(['FlyingPress\HTML', 'end_buffering']);
  }

  private static function end_buffering($content)
  {
    // Process only GET requests
    if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
      return $content;
    }

    // Skip optimizing if there is "no_optimize" query string
    // E.g.: https://domain.com/?no_optimize
    if (isset($_GET['no_optimize'])) {
      return $content;
    }

    // Probably not HTML, skip!
    if (!self::is_valid_buffer($content)) {
      return $content;
    }

    // Prevent cache for admin interfaces
    if (is_admin()) {
      return $content;
    }

    // Skip processing wp-login.php, .php pages etc
    if (self::is_invalid_page()) {
      return $content;
    }

    // Prevent cache if logged in
    if (is_user_logged_in() && !Config::$config['optimize_logged_in']) {
      return $content;
    }

    // Prevent excluded pages from caching & optimizing
    if (Cache::is_page_excluded()) {
      return $content;
    }

    if (Config::$config['license_status'] !== 'valid') {
      return $content;
    }

    $meta_tag = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
    $content = str_replace('</head>', "$meta_tag</head>", $content);

    $html = new \DiDom\Document();

    $html->loadHTML($content, LIBXML_HTML_NODEFDTD | LIBXML_SCHEMA_CREATE);

    @mkdir(FLYING_PRESS_CACHE_DIR, 0755, true);

    Font::add_display_swap_to_internal_styles($html);

    Font::add_display_swap_to_google_fonts($html);

    Font::optimize_google_fonts($html);

    Font::preload_fonts($html);

    CSS::minify($html);

    CSS::remove_unused($html);

    CSS::lazy_render($html);

    JavaScript::minify($html);

    JavaScript::delay_scripts($html);

    JavaScript::defer_external($html);

    JavaScript::defer_inline($html);

    Video::lazy_load($html);

    IFrame::add_youtube_placeholder($html);

    IFrame::lazy_load($html);

    Image::add_width_height($html);

    Image::exclude_above_fold($html);

    Image::responsive_images($html);

    Image::lazy_load($html);

    Image::preload($html);

    Image::lazy_load_bg_style($html);

    Image::lazy_load_bg_class($html);

    JavaScript::inject_core_lib($html);

    CDN::add_preconnect($html);

    CDN::rewrite($html);

    Preload::remove_page_from_preload();

    if (isset($_SERVER['HTTP_X_FLYING_PRESS_PRELOAD'])) {
      Preload::preload_url(Preload::get_next_url());
    }

    if (Cache::is_page_cacheable()) {
      if (defined('CLOSTE_APP_ID')) {
        header('X-Cacheable: yes', true);
      }
      Cache::cache_page($html);
    }

    return $html;
  }

  private static function is_valid_buffer($content)
  {
    // No HTML tag
    if (stripos($content, '<html') === false) {
      return false;
    }

    // No body closing tag
    if (stripos($content, '</body>') === false) {
      return false;
    }

    // XSL stylesheet found
    if (stripos($content, '<xsl:stylesheet') !== false) {
      return false;
    }

    // XSL stylesheet found
    if (stripos($content, '<xsl:stylesheet') !== false) {
      return false;
    }

    // Has no HTML5 doc type
    if (!preg_match('/^<!DOCTYPE.+html/i', ltrim($content))) {
      return false;
    }

    // AMP
    if (self::is_amp_markup($content)) {
      return false;
    }

    return true;
  }

  private static function is_amp_markup($content)
  {
    if (function_exists('is_amp_endpoint')) {
      return is_amp_endpoint();
    }

    $is_amp_markup = preg_match('/<html[^>]*(?:amp|⚡)/i', $content);

    return $is_amp_markup;
  }

  private static function is_invalid_page()
  {
    $current_url = home_url($_SERVER['REQUEST_URI']);
    $keywords = ['/wp-admin', '/feed', '.xml', '.txt', '.php'];
    array_push($keywords, wp_login_url());

    if (Utils::any_keywords_match_string($keywords, $current_url)) {
      return true;
    }
    return false;
  }
}
