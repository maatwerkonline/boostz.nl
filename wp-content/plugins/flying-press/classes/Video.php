<?php
namespace FlyingPress;

class Video
{
  public static function lazy_load($html)
  {
    $videos = $html->find('video');

    foreach ($videos as $video) {
      $video->preload = 'none';
    }
  }
}
