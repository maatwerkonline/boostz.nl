<?php
namespace FlyingPress;

class Image
{
  public static function init()
  {
    add_filter('wp_lazy_loading_enabled', '__return_false');
    add_action('wp_head', ['FlyingPress\Image', 'inject_height_auto']);
  }

  public static function inject_height_auto()
  {
    if (!Config::$config['img_width_height']) {
      return true;
    }
    echo '<style>img{height:auto}</style>';
  }

  public static function responsive_images($html)
  {
    if (!Config::$config['img_responsive']) {
      return true;
    }

    if (!Config::$config['flying_cdn']) {
      return true;
    }

    if (!Config::$config['cdn_enable']) {
      return true;
    }

    $site_url = FLYING_PRESS_SITE_URL;
    $images = $html->find("img[src^=$site_url]");
    foreach ($images as $image) {
      // Exclude SVG images
      if (Utils::any_keywords_match_string(['.svg'], $image->src)) {
        continue;
      }

      $image->removeAttribute('srcset');
      $image->removeAttribute('sizes');
      $image->loading = 'lazy';
      $image->{'data-origin-src'} = $image->src;
      $image->src = self::get_placeholder($image);
    }
  }

  public static function exclude_above_fold($html)
  {
    if (!Config::$config['img_lazyload_exclude_count']) {
      return true;
    }

    $site_url = FLYING_PRESS_SITE_URL;
    $images = $html->find("img[src^=$site_url]");

    foreach ($images as $index => $image) {
      $image->loading = 'eager';
      if ($index + 1 === Config::$config['img_lazyload_exclude_count']) {
        break;
      }
    }
  }

  public static function lazy_load($html)
  {
    if (!Config::$config['img_lazyload']) {
      return true;
    }

    $images = $html->find('img[src]');

    $default_exclude_keywords = [
      'eager',
      'skip-lazy',
      'data-src=',
      'data-srcset=',
      'data-no-lazy=',
      'data-lazy-original=',
      'data-lazy-src=',
      'data-lazysrc=',
      'data-lazyload=',
      'lazy-slider-img=',
      'class="ls-l',
      'class="ls-bg',
      'soliloquy-image',
    ];

    $user_exclude_keywords = Config::$config['img_lazyload_excludes'];
    $lazy_load_method = Config::$config['img_lazyload_method'];

    $placeholder =
      'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

    foreach ($images as $image) {
      if (Utils::any_keywords_match_string($user_exclude_keywords, $image)) {
        $image->loading = 'eager';
        continue;
      }

      if (Utils::any_keywords_match_string($default_exclude_keywords, $image)) {
        continue;
      }

      if ($lazy_load_method === 'native') {
        $image->loading = 'lazy';
      } else {
        $image->{'data-lazy-src'} = $image->src;
        $image->{'data-lazy-method'} = 'viewport';
        $image->{'data-lazy-attributes'} = 'src';
        $image->src = self::get_placeholder($image);
        if ($image->srcset) {
          $image->{'data-lazy-srcset'} = $image->srcset;
          $image->{'data-lazy-attributes'} = 'src,srcset';
          $image->removeAttribute('srcset');
        }
      }
    }
  }

  public static function lazy_load_bg_style($html)
  {
    if (!Config::$config['img_lazyload']) {
      return true;
    }

    $exclude_keywords = Config::$config['img_lazyload_excludes'];

    $elements = $html->find("*[style*='url']");

    foreach ($elements as $element) {
      if (Utils::any_keywords_match_string($exclude_keywords, $element)) {
        continue;
      }

      $element->{'data-lazy-style'} = $element->style;
      $element->{'data-lazy-method'} = 'viewport';
      $element->{'data-lazy-attributes'} = 'style';
      $element->removeAttribute('style');
    }
  }

  public static function lazy_load_bg_class($html)
  {
    if (!Config::$config['img_lazyload']) {
      return true;
    }

    $elements = $html->find('.lazy-bg');

    foreach ($elements as $element) {
      $element->{'data-lazy-id'} = $element->id;
      $element->{'data-lazy-class'} = $element->class;
      $element->{'data-lazy-method'} = 'viewport';
      $element->{'data-lazy-attributes'} = 'id,class';
      $element->removeAttribute('class');
      $element->removeAttribute('id');
    }
  }

  public static function add_width_height($html)
  {
    if (!Config::$config['img_width_height']) {
      return true;
    }

    $images = $html->find('img[!height],img[height=auto]');
    foreach ($images as $image) {
      $size = self::get_width_height($image->src);
      if ($size) {
        $image->width = $image->width ?? $size['width'];
        // When width is already set, we've to calculate height wrt to it
        $image->height = $image->width
          ? round(($image->width * $size['height']) / $size['width'])
          : $size['height'];
      }
    }
  }

  public static function preload($html)
  {
    if (!Config::$config['img_preload']) {
      return true;
    }

    $images = $html->find('img[loading=eager]');

    if (is_singular() && $html->has('img.wp-post-image[src]')) {
      $featured_image = $html->first('img.wp-post-image[src]');
      $featured_image->loading = 'eager';
      array_push($images, $featured_image);
    }

    foreach ($images as $image) {
      if (preg_match('/data:image/', $image->src)) {
        continue;
      }
      $image_tag = $html->createElement('link', null, [
        'rel' => 'preload',
        'href' => $image->src,
        'as' => 'image',
        'imagesrcset' => $image->srcset ?? null,
        'imagesizes' => $image->sizes ?? null,
      ]);
      $html->first('head')->insertAfter($image_tag, $html->first('title'));
    }
  }

  private static function get_width_height($url)
  {
    try {
      // Extract width if found the the url. For example something-100x100.jpg
      if (preg_match('/(?:.+)-([0-9]+)x([0-9]+)\.(jpg|jpeg|png|gif|svg)$/', $url, $matches)) {
        list($_, $width, $height) = $matches;
        return ['width' => $width, 'height' => $height];
      }

      $file_path = Cache::get_file_path_from_url($url);
      if (!is_file($file_path)) {
        return false;
      }

      if (pathinfo($file_path, PATHINFO_EXTENSION) === 'svg') {
        $xml = @simplexml_load_file($file_path);
        $attr = $xml->attributes();
        $viewbox = explode(' ', $attr->viewBox);
        $width =
          isset($attr->width) && preg_match('/\d+/', $attr->width, $value)
            ? (int) $value[0]
            : (count($viewbox) == 4
              ? (int) $viewbox[2]
              : null);
        $height =
          isset($attr->height) && preg_match('/\d+/', $attr->height, $value)
            ? (int) $value[0]
            : (count($viewbox) == 4
              ? (int) $viewbox[3]
              : null);
        if ($width && $height) {
          return ['width' => $width, 'height' => $height];
        }
      }

      // Get image size by checking the file
      list($width, $height) = getimagesize($file_path);
      if ($width && $height) {
        return ['width' => $width, 'height' => $height];
      }
    } catch (Exception $e) {
      return false;
    }
  }

  private static function get_placeholder($image)
  {
    if ($image->width && $image->height) {
      return $image->src = "data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' viewBox%3D'0 0 $image->width $image->height'%2F%3E";
    }
    return 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw';
  }
}
