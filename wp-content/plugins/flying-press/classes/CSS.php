<?php
namespace FlyingPress;

use Wa72\Url\Url;

class CSS
{
  public static function lazy_render($html)
  {
    $selectors = Config::$config['css_lazy_render_selectors'];
    array_push($selectors, '.lazy-render');
    $selectors = array_filter($selectors); // remove empty values
    $elements = $html->find(implode(',', $selectors));

    foreach ($elements as $element) {
      $element->style = "content-visibility:auto;contain-intrinsic-size:1px 1000px;$element->style";
    }
  }

  public static function remove_unused($html)
  {
    if (!Config::$config['css_extract_used']) {
      return true;
    }

    if (!Config::$config['css_minify']) {
      return true;
    }

    // Prevent removing unused css if user is logged in
    if (is_user_logged_in()) {
      return true;
    }

    $type = self::get_page_type();

    $critical_css_file_path = FLYING_PRESS_CACHE_DIR . $type . '.critical.css';
    $used_css_file_path = FLYING_PRESS_CACHE_DIR . $type . '.used.css';
    $used_css_file_url = FLYING_PRESS_CACHE_URL . $type . '.used.css';
    $unused_css_method = Config::$config['css_unused_method'];
    $styles = $html->find('link[rel="stylesheet"][href*=".css"]');

    if (!is_file($critical_css_file_path)) {
      $css_string = '';
      foreach ($styles as $style) {
        $file_path = Cache::get_file_path_from_url($style->href);
        if (!is_file($file_path)) {
          continue;
        }
        $css_string .= file_get_contents($file_path);
      }

      $scripts = $html->find('script[src]');
      foreach ($scripts as $script) {
        $file_path = Cache::get_file_path_from_url($script->src);
        if (!is_file($file_path)) {
          continue;
        }
        $script_content = file_get_contents($file_path);

        $node = $html->createTextNode($script_content);
        $script->appendChild($node);

        $script->dummy = null;
        $script->{'data-src'} = $script->src;
        $script->removeAttribute('src');
      }

      $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

      $response = wp_remote_post(FLYING_PRESS_API_URL . '/optimize-css', [
        'body' => [
          'html' => $html->html(),
          'css' => $css_string,
          'url' => $url,
          'config' => Config::$config,
        ],
        'timeout' => 60,
      ]);

      $scripts = $html->find('script[dummy]');
      foreach ($scripts as $script) {
        $script->setInnerHtml('');
        $script->removeAttribute('dummy');
        $script->src = $script->{'data-src'};
        $script->removeAttribute('data-src');
      }

      if (is_wp_error($response) || wp_remote_retrieve_response_code($response) != 200) {
        $critical_css = '/* Timeout or API call returned error */';
        $used_css = '/* Timeout or API call returned error */';
      } else {
        $css = json_decode($response['body'], true);
        $critical_css = $css['critical_css'];
        $used_css = $css['used_css'];
      }

      file_put_contents($critical_css_file_path, $critical_css);
      file_put_contents($used_css_file_path, $used_css);
    }

    foreach ($styles as $style) {
      switch ($unused_css_method) {
        case 'async':
          $style->media = 'print';
          $style->onload = "this.media='all'";
          break;
        case 'interaction':
          $style->{'data-lazy-method'} = 'interaction';
          $style->{'data-lazy-attributes'} = 'href';
          $style->{'data-lazy-href'} = $style->href;
          $style->removeAttribute('href');
          break;
        case 'remove':
          $style->remove();
          break;
      }
    }

    $critical_css = file_get_contents($critical_css_file_path);
    $critical_css_elem = $html->createElement('style', $critical_css, ['id' => 'flying-press-css']);
    $html->first('head')->insertAfter($critical_css_elem, $html->first('title'));

    if (is_file($used_css_file_path) && $unused_css_method !== 'async') {
      $hash = substr(hash_file('md5', $used_css_file_path), 0, 12);
      $hashed_used_css_file_name = "$hash.$type.used.css";
      $hashed_used_css_file_path = FLYING_PRESS_CACHE_DIR . "$hash.$type.used.css";
      $hashed_used_css_file_url = FLYING_PRESS_CACHE_URL . "$hash.$type.used.css";

      if (!is_file($hashed_used_css_file_path)) {
        copy($used_css_file_path, $hashed_used_css_file_path);
      }

      $used_css_elem = $html->createElement('link', null, [
        'rel' => 'stylesheet',
        'media' => 'print',
        'onload' => "this.media='all'",
        'href' => $hashed_used_css_file_url,
      ]);
      $html->first('head')->insertAfter($used_css_elem, $html->first('#flying-press-css'));
    }
  }

  public static function minify($html)
  {
    if (!Config::$config['css_minify']) {
      return true;
    }

    $styles = $html->find('link[rel=stylesheet][href]');

    foreach ($styles as $style) {
      // Convert URL to file path
      $file_path = Cache::get_file_path_from_url($style->href);
      if (!is_file($file_path)) {
        continue;
      }

      // Generate hash
      $css = file_get_contents($file_path);
      $hash = Config::$config['cdn_enable'] ? md5($css) : md5($css . Config::$config['cdn_url']);
      $file_name = substr($hash, 0, 12) . '.' . basename($file_path);
      $minified_path = FLYING_PRESS_CACHE_DIR . $file_name;
      $minified_url = FLYING_PRESS_CACHE_URL . $file_name;

      if (!is_file($minified_path)) {
        $minifier = new \MatthiasMullie\Minify\CSS();
        $minifier->add($file_path);
        $minified_css = $minifier->minify();
        $minified_css = self::rewrite_absolute_urls($minified_css, $style->href);
        $minified_css = Font::inject_display_swap($minified_css);
        CDN::rewrite($minified_css);

        // Inject media attribute to CSS as media query
        if ($style->media && $style->media != 'all') {
          $minified_css = "@media $style->media{{$minified_css}}";
          $style->media = 'all';
        }

        file_put_contents($minified_path, $minified_css);
      }

      $style->href = $minified_url;
    }
  }

  private static function get_page_type()
  {
    global $wp_query;
    $type = 'notfound';

    if ($wp_query->is_page) {
      $type = is_front_page() ? 'front' : 'page-' . $wp_query->post->ID;
    } elseif ($wp_query->is_home) {
      $type = 'home';
    } elseif ($wp_query->is_single && get_post_type()) {
      $type = get_post_type();
    } elseif ($wp_query->is_single) {
      $type = $wp_query->is_attachment ? 'attachment' : 'single';
    } elseif ($wp_query->is_category) {
      $type = 'category';
    } elseif ($wp_query->is_tag) {
      $type = 'tag';
    } elseif ($wp_query->is_tax) {
      $type = 'tax';
    } elseif ($wp_query->is_archive) {
      if ($wp_query->is_day) {
        $type = 'day';
      } elseif ($wp_query->is_month) {
        $type = 'month';
      } elseif ($wp_query->is_year) {
        $type = 'year';
      } elseif ($wp_query->is_author) {
        $type = 'author';
      } else {
        $type = 'archive';
      }
    } elseif ($wp_query->is_search) {
      $type = 'search';
    } elseif ($wp_query->is_404) {
      $type = 'notfound';
    }
    return $type;
  }

  private static function rewrite_absolute_urls($content, $base_url)
  {
    $regex = '/url\([\'"]*(.*?)[\'"]*\)/';

    $content = preg_replace_callback(
      $regex,
      function ($match) use ($base_url) {
        $url_string = $match[0];
        $relative_url = $match[1];
        $absolute_url = Url::parse($relative_url);
        $absolute_url->makeAbsolute(Url::parse($base_url));
        return str_replace($relative_url, $absolute_url, $url_string);
      },
      $content
    );

    return $content;
  }
}
