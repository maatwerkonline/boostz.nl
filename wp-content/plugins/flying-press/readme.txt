=== FlyingPress ===
Requires at least: 4.7
Tested up to: 5.8.2
Requires PHP: 7.2
Stable tag: 3.7.3

== Description ==
We make WordPress fly

== Changelog ==
= 3.7.3 - 02 December, 2021 =
- Fix: Appending hash to JS files having .json in URL

= 3.7.2 - 26 November, 2021 =
- Improvement: Keyless activation on plugin update
- Fix: Removed purge actions for non-admin (revert)

= 3.7.1 - 25 November, 2021 =
- Improvement: Purge actions are now available to editors and store managers

= 3.7.0 - 22 November, 2021 =
- New: Keyless activation - No need to enter license key!

= 3.6.13 - 02 November, 2021 =
- Improvement: Prevent minifying JS files when improvement is <10% or <2KB
- Improvement: Use absolute path for require_once

= 3.6.12 - 26 October, 2021 =
- Improvement: New cache lifespan of 6 hours

= 3.6.11 - 25 October, 2021 =
- Fix: Admin bar not visible when turning on optimize for logged in users

= 3.6.10 - 23 October, 2021 =
- Fix: Prevent removal of CDATA in internal styles
- Fix: PHP warnings when self-hosting some Google Fonts
- Improvement: Removed custom cache-control headers

= 3.6.9 - 22 October, 2021 =
- Fix: Encoding issues with special characters in internal CSS

= 3.6.8 - 14 October, 2021 =
- Fix: PHP warnings when images have unknown extensions
- Fix: Add billing address to card in FlyingCDN subscription
- Improvement: Updated list of default exclude keywords in image lazy loading

= 3.6.7 - 11 October, 2021 =
- Fix: Conflict when subfolder installation contains "wp" in folder name
- Fix: Prevent multiple images loading in Oxygen Gallery

= 3.6.6 - 05 October, 2021 =
- Fix: Credit card field missing in FlyingCDN subscription

= 3.6.5 - 05 October, 2021 =
- Fix: CSS minify not working when display swap is enabled after previous verion
- Improvement: Allow non-Indian American Express cards for FlyingCDN subscription

= 3.6.4 - 03 October, 2021 =
- Fix: Add display swap to internal css
- Improvement: Convert stylesheets with media attributes to media queries for better Critical CSS
- Improvement: Show FlyingCDN bandwidth usage with decimals

= 3.6.3 - 30 September, 2021 =
- Improvement: Support for Squirrly SEO sitemap
- Improvement: Fallback to /sitemap.xml when automatic detection fails
- Improvement: Updated internal libraries

= 3.6.2 - 29 September, 2021 =
- Fix: CDN rewrite not working with WMPL and subfolder installations

= 3.6.1 - 22 September, 2021 =
- Fix: Exclude SVG from responsive images
- Fix: Responsive images not loading with JavaScript lazy loading
- Improvement: Exclude scripts from defer also applies to inline JS

= 3.6.0- 10 September, 2021 =
- New: Responsive images using FlyingCDN
- Fix: Preload image from srcset if found

= 3.5.6 - 16 July, 2021 =
- Improvement: Use get_home_url() instead of get_site_url()
- Improvement: Allow exclude scripts from defer when inline defer is enabled
- Improvement: New documentation

= 3.5.5 - 21 June, 2021 =
- Improvement: Better CSS and JS files detection
- Improvement: Updated internal libraries
- Improvement: Hash CSS files with CDN url

= 3.5.4 - 14 June, 2021 =
- Fix: Escape strings in keywords list
- Improvement: Play button for YouTube placeholder
- Improvement: Support for Simple Custom CSS plugin

= 3.5.3 - 14 June, 2021 =
- Improvement: Use "/wp-admin" instead of "/wp-" from default cache exclude keywords

= 3.5.2 - 14 June, 2021 =
- Fix: Some iFrames not loading unless YouTube placeholder not enabled

= 3.5.1 - 12 June, 2021 =
- Fix: Support for youtube-nocookie.com YouTube embeds

= 3.5.0 - 12 June, 2021 =
- New: Use placeholder images for YouTube videos
- New: Self-host YouTube placeholder images
- Removed: Settings for lazy loading videos (will be enabled by default)
- Fix: Ignore empty keywords in list
- Fix: Incorrect ABSPATH is some hosting providers

= 3.4.0 - 07 June, 2021 =
- New: Enable or disable scripts to load on user interaction
- New: Only "safe" optimizations are enabled by default
- Fix: x-flying-press-source header will display LiteSpeed or Apache
- Fix: Use get_id() instead of ID for WooCommerce compatibility
- Improvement: Remove async attribute when defer is enabled
- Improvement: Minor UI improvements

= 3.3.2 - 04 June, 2021 =
- Fix: Manually excluded images from lazy load not preloaded
- Improvement: Disable 'Exclude Scripts from Defer' when Defer Inline is enabled
- Improvement: Remove resource hints to fonts.googleapis.com when self-hosting Google Fonts is enabled

= 3.3.1 - 31 May, 2021 =
- Improvement: Purge and preload WooCommerce products only when stock is increased/decreased

= 3.3.0 - 29 May, 2021 =
- New: Defer inline JavaScript
- Removed: Exclude jQuery from defer
- Removed: Fix render-blocking jQuery scripts
- Improvement: Better detection of CSS and JS files
- Fix: Purge and preload WooCommerce products when updated via Rest API
- Tweak: Added SG Optimizer to non-compatible plugins

= 3.2.0 - 19 May, 2021 =
- New: Enable beta versions
- Improvement: Register user interaction listeners only when needed

= 3.1.6 - 15 May, 2021 =
- Fix: Prevent preloading external images
- Fix: Prevent adding www on CDN rewrite incorrectly

= 3.1.5 - 14 May, 2021 =
- Fix: Use get_site_url instead of home_url for CDN rewrite
- Fix: Prevent errors parsing invalid SVGs

= 3.1.4 - 07 May, 2021 =
- Improvement: Update FlyingCDN credit card from settings page
- Improvement: Updated list of default ignore query strings

= 3.1.3 - 26 April, 2021 =
- Improvement: Better error messages when connecting custom subdomain fails in FlyingCDN
- Fix: Generate Critical & Used CSS for unknown post types
- Fix: Prevent self-hosting 404 Google Fonts
- Fix: Remove invalid whitespaces in Lazy Render selectors

= 3.1.2 - 11 April, 2021 =
- Fix: Prevent fixing render-blocking jQuery scripts when defer is disabled

= 3.1.1 - 08 April, 2021 =
- Fix: Exclude featured image from lazy loading

= 3.1.0 - 31 Mar, 2021 =
- New: Lazy Render! Skip rendering of elements until needed

= 3.0.7 - 26 Mar, 2021 =
- Improvement: Added user-agent "FlyingPress" while preloading
- Fix: Add width and height to images not uploaded using Media
- Fix: Add width and height to SVGs
- Fix: Fatal errors while installing in PHP 8

= 3.0.6 - 20 Mar, 2021 =
- Improvement: Refresh license information from dashboard

= 3.0.5 - 17 Mar, 2021 =
- Improvement: Add missing height of image with respect to existing width if found
- Fix: PHP 8 compatibility for self-hosting Google Fonts
- Fix: Duplicate UTF-8 meta tags

= 3.0.4 - 15 Mar, 2021 =
- Fix: Purge and preload posts only when post status is published

= 3.0.3 - 14 Mar, 2021 =
- Fix: HTML entity decoding issues with new parser

= 3.0.2 - 11 Mar, 2021 =
- Fix: Encoding of non-english characters and many other bug fixes in new parser engine
- Fix: Warnings generated from Preload.php

= 3.0.1 - 06 Mar, 2021 =
- Fix: Patched many bugs with new parser engine
- Fix: Enforce CDN rewriting only to static files
- Fix: Prevented some warnings while preloading pages 
- Improvement: Lazy loading background images now include id attribute
- Improvement: Added touch move and mouse wheel events to load on user interaction

= 3.0.0 - 01 Mar, 2021 =
- New: New HTML parsing engine!
- Improvement: 2x cache preload time
- Improvement: 5x-10x lower server resource usage
- Improvement: Notifications after saving settings now floats above all
- Tweak: Enable adding width and height attributes by default
- Tweak: Added common list of 3rd party scripts to load on user interaction
- Fix: Use WP_CONTENT_URL and WP_CONTENT_DIR constants instead of hard-coded values
- Fix: Prevent base64 images from preloading
- Fix: Preload only first feature image
- Fix: Lazy loading iFrames added using Thrive Architect
- Fix: Overwrite existing font-display to enable swap when fallback font enabled

= 2.13.1 - 17 Feb, 2021 =
- Tweak: Increased user interaction timeout to 10s
- Fix: Prevent preloading cache after saving settings when cache is disabled
- Fix: Prevent self-hosting Google Fonts when there is an error in response
- Fix: Remove preconnects to fonts.gstatic.com when self-hosting Google Fonts
- Fix: Prevent base64 images from preload

= 2.13.0 - 08 Feb, 2021 =
- Tweak: Remove self-hosting internal CSS
- Fix: Add gzip when not enabled in server
- Fix: Prevent parsing of HTML twice

= 2.12.0 - 05 Feb, 2021 =
- New: Auto purge Varnish cache
- New: Added hooks after purging cache (for 3rd party integrations)
- Tweak: Default settings - switched lazy loading to Browser Native
- Tweak: Default settings - disabled exclude jQuery from defer
- Tweak: Default settings - enabled fix render-blocking jQuery Scripts
- Tweak: Generate Critical & Used CSS only when CSS Minify is enabled

= 2.11.0 - 04 Feb, 2021 =
- New: Support for Multisites

= 2.10.0 - 31 Jan, 2021 =
- New: Auto preload images excluded from lazy loading
- Tweak: Disable WordPress inbuilt lazy loading
- Fix: Incorrect icon in Cache settings

= 2.9.5 - 29 Jan, 2021 =
- Fix: Some fonts are not delivered from CDN

= 2.9.4 - 25 Jan, 2021 =
- Tweak: Updated cookie list for bypassing cache
- Tweak: Improved WPML compatibility 

= 2.9.3 - 25 Jan, 2021 =
- Tweak: Improved performance of purging cache
- Fix: Convert URLs from relative to absolute in CSS imports

= 2.9.2 - 23 Jan, 2021 =
- New: Cache lifespan of 2 hours
- Fix: License expiry date showing as invalid in Safari browser
- Fix: Adding incorrect hash to JS files ending with .min.js

= 2.9.1 - 22 Jan, 2021 =
- Tweak: Generate only hash for .min.js files instead of minifying (?ver=hash)
- Fix: Error in minified CSS files containing SVG background images

= 2.9.0 - 21 Jan, 2021 =
- New: Auto change hash of minified files when CDN is enabled/disabled
- New: Minify JS files having .min.js extension

= 2.8.3 - 17 Jan, 2021 =
- Fix: Mixed content error when using SSL from proxy

= 2.8.2 - 16 Jan, 2021 =
- Fix: Converting relative urls to absolute urls

= 2.8.1 - 15 Jan, 2021 =
- Fix: Not rewriting to CDN in preload tag

= 2.8.0 - 07 Jan, 2021 =
- New: Force include CSS selectors in Critical & Used CSS
- New: Added UTF-8 encoding for cached pages
- Fix: Empty imagesrcset and imagesizes on preload tag
- Fix: Exclude images not respecting background images
- Tweak: UI improvements

= 2.7.7 - 25 Dec, 2020 =
- Fix: PHP 8 compatibility

= 2.7.6 - 18 Dec, 2020 =
- Fix: Process smaller inline styles for critical/used css

= 2.7.5 - 18 Dec, 2020 =
- Fix: Remove unwanted style tag after adding used css

= 2.7.4 - 12 Dec, 2020 =
- Fix: Process JS files while generating critical/used css

= 2.7.3 - 11 Dec, 2020 =
- Fix: Removed Jilt cookie for bypassing cache

= 2.7.2 - 11 Dec, 2020 =
- Fix: Exclude mobile specific stylesheets from critical/used css
- Tweak: Increase user interaction timeout from 5s to 6s
- Tweak: Minor UI improvements

= 2.7.1 - 07 Dec, 2020 =
- Fix: Add cache-control headers while serving cached pages (to prevent any browser caching)
- Tweak: Added 'Save and Optimize Now' button in Database Cleaner
- Tweak: Prevent Amex Cards from subscribing to FlyingCDN (not supported by payment gateway)

= 2.7.0 - 04 Dec, 2020 =
- New: Database Cleaner
- Tweak: Minor UI improvements
- Fix: Detect dynamic classes from delayed JS files
- Fix: Continue serving page on parsing failure

= 2.6.6 - 23 Nov, 2020 =
- Fix: Fallback to "/sitemap.xml" when no comptabile sitemap/seo plugins are found
- Tweak: Form validation while subscribing to FlyingCDN
- Tweak: Minor copy updates

= 2.6.5 - 18 Nov, 2020 =
- Fix: Purge shop & category pages on adding/updating products

= 2.6.4 - 16 Nov, 2020 =
- Fix: Purge product page automatically when stock status changes

= 2.6.3 - 06 Nov, 2020 =
- Fix: Add CDN to images with webp & ico extensions
- Fix: Optimize only GET requests

= 2.6.2 - 04 Nov, 2020 =
- Tweak: Support for Cache reports in closte.com
- Tweak: Prevent optimizing pages that are excluded from caching

= 2.6.1 - 30 Oct, 2020 =
- Fix: Prevent adding height and width when it's a SVG image

= 2.6.0 - 30 Oct, 2020 =
- New: Add missing width & height attributes to images
- New: Separate options to purge CSS/JS/Fonts and Critical/Used CSS
- Tweak: Changed default image lazy loading method to JavaScript
- Tweak: Allow 'space' character in keyword input fields
- Tweak: Updated cookie list to bypass cache
- Tweak: Confirmation before purging Critical/Used CSS
- Tweak: Increased Critical/Used CSS generation API timeout
- Tweak: UI improvements

= 2.5.1 - 24 Oct, 2020 =
- Tweak: Improvements in 'Fix Render-Blocking jQuery Scripts'

= 2.5.0 - 23 Oct, 2020 =
- New: Ignore custom query strings
- Fix: Only preload images from origin site
- Fix: Prevent preloading all features images in archives

= 2.4.0 - 22 Oct, 2020 =
- New: Preload critical images
- New: Cache Lifespan - Automatically purge and preload cache after a lifespan
- Tweak: Disable optimize for logged in users by default

= 2.3.4 - 17 Oct, 2020 =
- Fix: Display sitemap warning only when cache is enabled

= 2.3.3 - 17 Oct, 2020 =
- Fix: Use 'stylesheet' with 'media' insted of 'preload' for asynchronous CSS files

= 2.3.2 - 17 Oct, 2020 =
- Fix: Prevent optimizing custom login pages

= 2.3.1 - 17 Oct, 2020 =
- New: Support for Companion Sitemap Generator
- Fix: Serve cached pages for HEAD request
- Fix: Missing id warnings in admin bar menu

= 2.3.0 - 15 Oct, 2020 =
- New: Purge current page
- New: View site without any optimization (?no_optimize)
- New: Support for Jilt cookies
- Fix: Undefined index warnings

= 2.2.9 - 13 Oct, 2020 =
- Prevent optimizing .php pages

= 2.2.8 - 13 Oct, 2020 =
- Improvements in auto purge and preload
- Optimize pages excluded from caching
- Optimizer Engine API updates, timeout and size limits

= 2.2.7 - 09 Oct, 2020 =
- Optimizer Engine API updates

= 2.2.6 - 09 Oct, 2020 =
- Bug fix - 'Purge CSS/JS' not working when cache is disabled

= 2.2.5 - 09 Oct, 2020 =
- UI improvements
- Updates to some default settings

= 2.2.4 - 07 Oct, 2020 =
- Fixed bugs in auto purge & preload

= 2.2.3 - 06 Oct, 2020 =
- Use domain name as cache key, fixes redirection issues in www and non-www

= 2.2.2 - 05 Oct, 2020 =
- Fixed rewriting URLs to CDN URL in self-hosted Google Fonts

= 2.2.1 - 04 Oct, 2020 =
- Fixed missing 'Dashboard' link in admin bar while viewing site

= 2.2.0 - 03 Oct, 2020 =
- Preload fonts - Prioritize loading fonts that required immediately for the render
- Additional auto purge - purge pages when a post is published/updated
- Preload cache automatically after post is published/updated
- UI improvements

= 2.1.8 - 02 Oct, 2020 =
- Generate hashed file name for used CSS

= 2.1.7 - 01 Oct, 2020 =
- Process inline styles for generating critical/used CSS
- Removing jQuery migrate option
- Automatically exclude login pages from caching
- Bug fixes in lazy loading self-hosted videos

= 2.1.6 - 30 Sept, 2020 =
- Expiry headers for JavaScript files
- Add display:swap to generated used css

= 2.1.5 - 29 Sept, 2020 =
- Fixed bugs in self-hosting Google Fonts
- Prevent purging cached pages on updating FlyingCDN bandwidth
- Automatically preload cache on saving settings
- Show purge option in admin bar when cache is disabled

= 2.1.4 - 27 Sept, 2020 =
- Optimizer Engine API update
- Removing unwanted logs

= 2.1.3 - 27 Sept, 2020 =
- Optimizer Engine API update

= 2.1.2 - 27 Sept, 2020 =
- Optimizer Engine API update

= 2.1.1 - 27 Sept, 2020 =
- Disabled HTML minify (fixes minifying code blocks)
- Updated Optimizer engine API
- Increased memory limits for HTML parsing
- Increased critical & used CSS generation timeout
- Added version number to dashboard

= 2.1.0 - 26 Sept, 2020 =
- Generate separate critical CSS and 'used' CSS
- Removed minifying and separating inline styles
- Automatically purge blog archive page
- New Facebook group link for FlyingPress community
- Removed roadmap

= 2.0.5 - 13 Sept, 2020 =
- Add htaccess rules just above WordPress rules

= 2.0.4 - 13 Sept, 2020 =
- Fixed rendering of images when using JS lazy load without srcset

= 2.0.3 - 13 Sept, 2020 =
- Append used CSS and preconnect tag only after title tag
- Fixed CDN rewrite in inline CSS
- Fixed absolute path conversion in inline CSS

= 2.0.2 - 11 Sept, 2020 =
- Prevent caching 404 pages

= 2.0.1 - 10 Sept, 2020 =
- Prevent purging entire cache on plugin/theme/core updates
- Auto refresh cached pages on visiting 'Dashboard'
- Adding FAQs for 'Cache'

= 2.0.0 - 10 Sept, 2020 =
- Generate cache locally
- Speed up cache generation by around 10x
- Purge cached pages (HTML files) alone
- Support server side caching layers by disabling inbuilt cache
- Automatically exclude WooCommerce cart, checkout, account page from caching
- Exclude pages from caching
- Caching without having a sitemap
- Detect native sitemap
- Optimize for logged in users
- Lazy load videos
- Other bugs fixes and improvements

= 1.0.8 - 10 Aug, 2020 =
- Changelog shortcut in plugins list

= 1.0.7 - 10 Aug, 2020 =
- Fixed the bug which caused issues in Classic Editor when emoji is disabled

= 1.0.6 - 09 Aug, 2020 =
- Purge individual pages
- Show FlyingPress settings in admin bar only to admins

= 1.0.5 - 05 Aug, 2020 =
- Fixed plugin crash in some PHP versions

= 1.0.4 - 05 Aug, 2020 =
- Display cache status of each post/page

= 1.0.3 - 04 Aug, 2020 =
- Updated FB community link to WP Speed Matters FB group
- Typo fixes

= 1.0.2 - 03 Aug, 2020 =
- Provide detailed error message when subscribing to CDN failed
- Fixed loading spinner stuck after cancelling CDN subscription

= 1.0.1 - 02 Aug, 2020 =
- Allow text inputs instead of numbers for zip/post codes in CDN subscription form
- Prevent 404 in home page when order placed in Easy Digital Downloads
- Fixed bugs causing files not writing in htaccess and advanced-cache.php
- Typo fixes

= 1.0.0 - 31 Jul, 2020 =
- Stable release!