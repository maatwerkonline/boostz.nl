<?php
/**
 * Plugin Name: FlyingPress
 * Plugin URI: https://flying-press.com
 * Description: We make WordPress fly
 * Version: 3.7.3
 */

defined('ABSPATH') or die('No script kiddies please!');

$host = parse_url(get_home_url(), PHP_URL_HOST);
define('FLYING_PRESS_VERSION', '3.7.3');
define('FLYING_PRESS_HOME_URL', home_url());
define('FLYING_PRESS_SITE_URL', site_url());
define('FLYING_PRESS_FILE', plugin_basename(__FILE__));
define('FLYING_PRESS_FILE_PATH', __FILE__);
define('FLYING_PRESS_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('FLYING_PRESS_PLUGIN_URL', plugin_dir_url(__FILE__));
define('FLYING_PRESS_CACHE_DIR', WP_CONTENT_DIR . "/cache/flying-press/$host/");
define('FLYING_PRESS_CACHE_URL', WP_CONTENT_URL . "/cache/flying-press/$host/");
define('FLYING_PRESS_ABSPATH', str_replace(wp_basename(WP_CONTENT_DIR), '', WP_CONTENT_DIR));
define('FLYING_PRESS_API_URL', 'https://api.flying-press.com');
define('FLYING_PRESS_EDD_STORE_URL', 'https://flying-press.com');
define('FLYING_PRESS_EDD_ITEM_ID', 38);

require_once dirname(__FILE__) . '/vendor/autoload.php';

@include 'license-data.php';

FlyingPress\Config::init();
FlyingPress\Settings::init();
FlyingPress\Shortcuts::init();
FlyingPress\HTML::init();
FlyingPress\Ajax::init();
FlyingPress\Cache::init();
FlyingPress\Adminbar::init();
FlyingPress\Lifecycle::init();
FlyingPress\Purge::init();
FlyingPress\Bloat::init();
FlyingPress\License::init();
FlyingPress\Updater::init();
FlyingPress\Compatibility::init();
FlyingPress\Image::init();
FlyingPress\JavaScript::init();
FlyingPress\Database::init();
FlyingPress\Varnish::init();