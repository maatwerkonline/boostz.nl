const show_loading = () => {
  document.getElementById('fp-icon').style.display = 'none';
  document.getElementById('fp-loading-icon').style.display = 'block';
};

const hide_loading = () => {
  document.getElementById('fp-icon').style.display = 'block';
  document.getElementById('fp-loading-icon').style.display = 'none';
};

// Purge cached pages
const purge_cached_pages = () => {
  show_loading();
  fetch(window.ajaxurl, {
    method: 'POST',
    body: new URLSearchParams('action=purge_cached_pages'),
  }).finally(() => hide_loading());
};

// Preload current page
const purge_current_page = () => {
  show_loading();
  fetch(window.ajaxurl, {
    method: 'POST',
    body: new URLSearchParams('action=purge_current_page&url=' + window.location.href),
  }).finally(() => hide_loading());
};

// Preload cache
const preload_cache = () => {
  show_loading();
  fetch(window.ajaxurl, {
    method: 'POST',
    body: new URLSearchParams('action=preload_cache'),
  }).finally(() => hide_loading());
};

// Purge CSS, JS and Fonts
const purge_css_js_fonts = () => {
  show_loading();
  fetch(window.ajaxurl, {
    method: 'POST',
    body: new URLSearchParams('action=purge_css_js_fonts'),
  }).finally(() => hide_loading());
};

// Purge entire cache (including Critical/Used CSS)
const purge_entire_cache = () => {
  const confirm = window.confirm(
    'Purging will regenerate Critical/Used CSS. This will result in slow page load time for the first visit and increase cache preload time. Are you sure?',
  );
  if (!confirm) return;
  show_loading();
  fetch(window.ajaxurl, {
    method: 'POST',
    body: new URLSearchParams('action=purge_entire_cache'),
  }).finally(() => hide_loading());
};
